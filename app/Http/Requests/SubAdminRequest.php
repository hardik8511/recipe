<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class SubAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unless = "change_status";
        $id = (!empty(Route::current()->parameters()['subadmin']->id) ? Route::current()->parameters()['subadmin']->id : NULL);
        return [
            'full_name'        =>  'required_unless:action,'.$unless.'|max:50',
            'email'             =>  'required_unless:action,'.$unless.'|max:150|unique:sub_admins,email,'.$id.',id,deleted_at,NULL',
            'contact_no'        =>  'nullable|min:6|max:16|unique:users,contact_no,'.$id.',id,deleted_at,NULL',
            'profile_photo'     =>  'nullable|mimes:jpeg,jpg,png',
        ];
    }
}
