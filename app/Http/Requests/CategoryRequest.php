<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd("hi");
        $unless = "change_status";
        $id = (!empty(Route::current()->parameters()['category']->id) ? Route::current()->parameters()['category']->id : NULL);
        // dd($id);

        return [
            'name'  =>  'required_unless:action,' . $unless . '|max:50|unique:categories,name,' . $id . '',
        ];
    }
}
