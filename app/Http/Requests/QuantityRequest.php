<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class QuantityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unless = "change_status";
        $id = (!empty(Route::current()->parameters()['quantity']->id) ? Route::current()->parameters()['quantity']->id : NULL);
        // dd($id);
        return [
            'no' =>  'required_unless:action,' . $unless . '|max:50|unique:quantity,no,'.$id.',id',
        ];
    }
}
