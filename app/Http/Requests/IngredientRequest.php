<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class IngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unless = "change_status";
        // dd(Route::current()->parameters());
        $id = (!empty(Route::current()->parameters()['ingredient']->id) ? Route::current()->parameters()['ingredient']->id : NULL);
        return [
            'name' =>  'required_unless:action,' . $unless . '|max:50|unique:ingredients,name,' . $id,
        ];
    }
}
