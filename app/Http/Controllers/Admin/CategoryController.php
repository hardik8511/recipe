<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\SubAdmin;
use App\Models\SubCategory;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("hiiiit5");
        return view("admin.pages.category.index", ['custom_title' => 'Categories']);

        // return view("admin.pages.category.create", ['custom_title' => 'Category']);
        //
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $records = [];
        $categories = Category::with("subcategories")->orderBy($sort_column, $sort_order);

        if ($search != '') {
            $categories->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%")
                    ->orWhereHas('subcategories', function ($q) use ($search) {
                        $q->where('name', 'like', "%{$search}%");
                    });
            });
        }

        $count = $categories->count();
        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $categories = $categories->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $categories = $categories->get();
        // dd($categories);
        foreach ($categories as $category) {
            // dd($category);

            // dd($category->subcategories[0]->name);
            $sub_category = "";
            if ($category->subcategories) {
                foreach ($category->subcategories as $value) {
                    # code...
                    $sub_category .=  $value->name . ",";
                }
            }
            $params = [
                'checked' => ($category->is_active == 'y' ? 'checked' : ''),
                'getaction' => $category->is_active,
                'class' => '',
                'id' => $category->custom_id,
            ];

            $records['data'][] = [
                'id' => $category->id,
                'name' => $category->name ?? "",
                // 'sub_category' => $category->subcategories ? $category->subcategories->name : "--",
                'sub_category' => rtrim($sub_category, ","),
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'Category', 'id' => $category->custom_id], $category)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $category->custom_id)->render(),
            ];
        }

        // dd($records);
        return $records;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.category.create')->with(['custom_title' => 'Category']);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        //
        $data = [
            "name" => "required|min:3",
            "sub_category_name.*" => 'required|min:2',
        ];
        $message = [
            'name.required' => "category name is required",
            'sub_category_name.*.required' => "sub category field is required",
        ];
        $is_validate = Validator::make($request->all(), $data, $message);
        if ($is_validate->fails()) {
            flash($is_validate->errors()->first())->error();
            return redirect()->back();
        }

        // dd($request->input());
        try {
            $category_data = new Category();
            $category_data->name = trim($request->name);
            $category_data->custom_id = getUniqueString("categories");
            if ($category_data->save()) {
                // dd($request->input());
                // // inserting the sub category 
                foreach ($request->sub_category_name as  $value) {
                    $sub_category = new SubCategory();
                    $sub_category->name = trim($value);
                    $sub_category->category_id = $category_data->id;
                    $sub_category->save();
                }
            }
            flash("category and sub category add successfully")->success();
            return redirect()->route("admin.category.index");
        } catch (Exception $ex) {
            flash($ex->getMessage())->error();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        // dd($category);
        if ($category) {
            # code...
            $sub_category_data = SubCategory::where("category_id", $category->id)->get();
            // dd($sub_category_data);
        } else {
            $sub_category_data = "";
        }
        // return view('admin.pages.category.edit', compact('category'))->with(['custom_title' => 'Category']);
        return view('admin.pages.category.edit', ["category" => $category, "sub_category_data" => $sub_category_data])->with(['custom_title' => 'Category']);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        try {
            DB::beginTransaction();
            if (!empty($request->action) && $request->action == 'change_status') {
                $content = ['status' => 204, 'message' => "something went wrong"];
                if ($category) {
                    $category->is_active = $request->value;
                    if ($category->save()) {
                        DB::commit();
                        $content['status'] = 200;
                        $content['message'] = "status updated successfully.";
                    }
                }
                return response()->json($content);
            } else {
                $category->name = $request->name;
                if ($category->save()) {
                    // dd($request->sub_category_name);
                    if ($request->sub_category_name) {
                        $delete_old = SubCategory::where("category_id", $category->id)->delete();

                        foreach ($request->sub_category_name as $key => $name_sub_category) {
                            $sub_category_data = new SubCategory();
                            $sub_category_data->name = $name_sub_category;
                            $sub_category_data->category_id = $category->id;
                            $sub_category_data->save();
                            // DB::enableQueryLog();
                            // $data =  SubCategory::firstOrCreate(
                            //     ['name' =>  $name_sub_category, 'category_id' => $category->id],
                            //     ['name' =>  $name_sub_category, 'category_id' => $category->id]
                            // );
                            // dd(DB::getQueryLog());
                            // dd($data);
                        }
                    }
                    DB::commit();
                    flash('category details updated successfully!')->success();
                } else {
                    flash('Unable to update category. Try again later')->error();
                }
                return redirect(route('admin.category.index'));
            }
        } catch (QueryException $e) {
            DB::rollback();

            flash($e->getMessage())->error();
            return redirect()->back()->flash('error', $e->getMessage());
        } catch (Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            Category::whereIn('custom_id', explode(',', $request->ids))->delete();
            $content['status'] = 200;
            $content['message'] = "category deleted successfully.";
            $content['count'] = Category::all()->count();
            return response()->json($content);
        } else {
            $user = Category::where('custom_id', $id)->firstOrFail();
            $user->delete();
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "category deleted successfully.", 'count' => Category::all()->count());
                return response()->json($content);
            } else {
                flash('category deleted successfully.')->success();
                return redirect()->route('admin.category.index');
            }
        }
        //
    }
}
