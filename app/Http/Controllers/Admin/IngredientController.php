<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\IngredientRequest;
use App\Models\Admin\Ingredients;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.ingredient.index')->with(['custom_title' => 'Ingredient']);
        //
    }
    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $records = [];
        $ingredients = Ingredients::orderBy($sort_column, $sort_order);

        if ($search != '') {
            $ingredients->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%");
            });
        }

        $count = $ingredients->count();
        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $ingredients = $ingredients->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $ingredients = $ingredients->get();
        foreach ($ingredients as $ingredient) {
            // dd($ingredient);
            $params = [
                'checked' => ($ingredient->is_active == 'y' ? 'checked' : ''),
                'getaction' => $ingredient->is_active,
                'class' => '',
                'id' => $ingredient->custom_id,
            ];

            $records['data'][] = [
                'id' => $ingredient->id,
                'name' => $ingredient->name ?? "",
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'Ingredient', 'id' => $ingredient->custom_id], $ingredient)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $ingredient->custom_id)->render(),
            ];
        }
        // dd($records);
        return $records;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.ingredient.create')->with(['custom_title' => 'Ingredient']);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientRequest $request)
    {
        $request['custom_id']   =   getUniqueString('ingredients');
        $user = Ingredients::create($request->all());
        if ($user->save()) {
            flash('Ingredient created successfully!')->success();
        } else {
            flash('Unable to save ingredient. Please try again later.')->error();
        }
        return redirect(route('admin.ingredients.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingredients $ingredient)
    {

        return view('admin.pages.ingredient.edit', compact('ingredient'))->with(['custom_title' => 'Ingredient']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // UserRequest $request, User $user
    public function update(IngredientRequest $request, Ingredients $ingredient)
    {

        try {
            DB::beginTransaction();
            if (!empty($request->action) && $request->action == 'change_status') {
                $content = ['status' => 204, 'message' => "something went wrong"];
                if ($ingredient) {
                    $ingredient->is_active = $request->value;
                    if ($ingredient->save()) {
                        DB::commit();
                        $content['status'] = 200;
                        $content['message'] = "Status updated successfully.";
                    }
                }
                return response()->json($content);
            } else {
                $ingredient->fill($request->all());
                if ($ingredient->save()) {
                    DB::commit();
                    flash('ingredient details updated successfully!')->success();
                } else {
                    flash('Unable to update ingredient. Try again later')->error();
                }
                return redirect(route('admin.ingredients.index'));
            }
        } catch (QueryException $e) {
            DB::rollback();
            flash('error', $e->getMessage());
            return redirect()->back();
        } catch (Exception $e) {
            flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];


            Ingredients::whereIn('custom_id', explode(',', $request->ids))->delete();
            $content['status'] = 200;
            $content['message'] = "ingredients deleted successfully.";
            $content['count'] = Ingredients::all()->count();
            return response()->json($content);
        } else {
            $ingredient = Ingredients::where('custom_id', $id)->firstOrFail();
            $ingredient->delete();
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "ingredients deleted successfully.", 'count' => Ingredients::all()->count());
                return response()->json($content);
            } else {
                flash('ingredients deleted successfully.')->success();
                return redirect()->route('admin.ingredients.index');
            }
        }
    }
}
