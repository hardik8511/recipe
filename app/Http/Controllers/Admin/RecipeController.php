<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Ingredients;
use App\Models\Admin\MeasurMent;
use App\Models\Admin\Quantity;
use App\Models\Category;
use App\Models\Image;
use App\Models\Recipe;
use App\Models\RecipeDirection;
use App\Models\RecipeIngredients;
use App\Models\SubCategory;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has("is_add_more")) {
            $page = $request->has('page') ? $request->get('page') : 1;
            $limit = $request->has('limit') ? $request->get('limit') : 3;
            $total_records = Recipe::count();
            $total_page = ceil($total_records / $limit);

            //calculate last page logic 
            if ($total_page <= $page + 1) {
                $is_last_page = "yes";
            } else {
                $is_last_page = "no";
            }

            $recipies = Recipe::with("subcategory", "image", 'favorites', 'likes', 'dislikes')->limit($limit)->offset(($page - 1) * $limit)->get();
            $after_render_recipe = view('admin.pages.recipe.recipe_load_more', compact('recipies', 'page', "is_last_page"))->render();
            return $after_render_recipe;
        } else {
            $page = $request->has('page') ? $request->get('page') : 1;
            $limit = $request->has('limit') ? $request->get('limit') : 6;
            $total_records = Recipe::count();
            $total_page = ceil($total_records / $limit);

            $recipies = Recipe::with("subcategory", "image", 'favorites', 'likes', 'dislikes')->limit($limit)->offset(($page - 1) * $limit)->get();
            return view('admin.pages.recipe.index', compact('recipies', "total_page", "page"))->with(['custom_title' => 'Recipe']);
        }
    }

    public function listing(Request $request)
    {
        // dd($request->input());
        extract($this->DTFilters($request->all()));
        $records = [];
        $recipies_data = Recipe::with("subcategory")->orderBy($sort_column, $sort_order);

        if ($search != '') {
            $recipies_data->where(function ($query) use ($search) {
                $query->where('title', 'like', "%{$search}%")
                    ->orWhere('preperation_time', 'like', "%{$search}%")
                    ->orWhere('cooking_time', 'like', "%{$search}%")
                    ->orWhereHas('subcategory', function ($q) use ($search) {
                        $q->where('name', 'like', "%{$search}%");
                    });
            });
        }

        $count = $recipies_data->count();

        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $recipies_data = $recipies_data->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $recipies_data = $recipies_data->get();
        foreach ($recipies_data as $recipe_item) {
            $params = [
                'checked' => ($recipe_item->is_active == 'y' ? 'checked' : ''),
                'getaction' => $recipe_item->is_active,
                'class' => '',
                'id' => $recipe_item->custom_id,
            ];

            $records['data'][] = [
                'id' => $recipe_item->id,
                'title' => $recipe_item->title ?? "",
                'preperation_time' => $recipe_item->preperation_time ?? "-",
                'cooking_time' => $recipe_item->cooking_time ?? "-",
                'sub_category_id' => $recipe_item->subcategory ?  $recipe_item->subcategory->name : "N/A",
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'Recipe', 'id' => $recipe_item->custom_id], $recipe_item)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $recipe_item->custom_id)->render(),
            ];
        }
        return $records;
    }

    public function create()
    {
        //
        $recipe_ingredient_data = Ingredients::get();
        $recipe_measurment_data = MeasurMent::get();
        $recipe_quantity_data = Quantity::get();
        $sub_category_data = Category::select('name', 'id')->with('subcategories')->get();

        return view('admin.pages.recipe.create', ["recipe_ingredient_data" => $recipe_ingredient_data, "recipe_measurment_data" => $recipe_measurment_data, "recipe_quantity_data" => $recipe_quantity_data, "form_step" => "step_one", "sub_category_data" => $sub_category_data])->with(['custom_title' => 'Recipe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->form_step == "step_one") {
            $request['custom_id'] = getUniqueString('recipes');
            try {
                // dd($request->input());
                DB::beginTransaction();
                // $last_request = $request->all();
                $request["admin_id"] = auth()->user()->id;
                // dd($request->input());
                $total_hour_minutes = substr($request->total_time, 0, 1) * 60;
                $total_minutes = (int)substr($request->total_time, strpos($request->total_time, ":") + 1);
                $request['total_time'] = $total_hour_minutes + $total_minutes;
                $recipe_data = Recipe::create($request->all());
                DB::commit();

                $path = storage_path('app/public/recipe_img/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }

                if (!empty($request->recipe_img)) {
                    foreach ($request->recipe_img as $key => $value) {
                        $file_name = time() . mt_rand(1, 99999) . "." . $value->getClientOriginalExtension();
                        $is_moved = $value->move($path, $path . $file_name);
                        if ($is_moved) {
                            Image::create([
                                "name" => $file_name,
                                "recipe_id" => $recipe_data->id,
                            ]);
                        } else {
                            continue;
                        }
                    }
                }

                flash('recipe created successfully!')->success();
                return redirect()->route("admin.recipe.edit", ["recipe" => $recipe_data->custom_id])->with("form_step_data", "step_two_form");
            } catch (QueryException $e) {
                DB::rollback();
                flash('error', $e->getMessage());
                return redirect()->back();
            } catch (Exception $ex) {
                flash($ex->getMessage())->error();
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($recipe->images->isNotEmpty());
        // dd($id);
        // DB::enableQueryLog();
        $recipe = Recipe::with('ingredients.ingredient')->where("custom_id", $id)->first();
        // dd(DB::getQueryLog());
        // dd($recipe);
        return view("admin.pages.recipe.view", compact("recipe"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Recipe $recipe)
    {
        $recipe_ingredient_data = Ingredients::get();
        $recipe_measurment_data = MeasurMent::get();
        $recipe_quantity_data = Quantity::get();
        $all_recipe_ingredient_data = RecipeIngredients::where("recipe_id", $recipe->id)->get();
        $recipe_direction_data = RecipeDirection::where("recipe_id", $recipe->id)->get();
        $recipe_images = Image::where("recipe_id", $recipe->id)->get();
        $sub_category_data = Category::select("name", "id")->with("subcategories")->get();
        // dd($sub_category_data);
        // dd($recipe_images);
        // dd($all_recipe_ingredient_data);
        return view('admin.pages.recipe.edit', ["recipe_ingredient_data" => $recipe_ingredient_data, "recipe_measurment_data" => $recipe_measurment_data, "recipe_quantity_data" => $recipe_quantity_data, "form_step" =>  "step_one", "recipe_data" => $recipe, 'all_recipe_ingredient_data' => $all_recipe_ingredient_data, "recipe_direction_data" => $recipe_direction_data, "recipe_images" => $recipe_images, "sub_category_data" => $sub_category_data])->with(['custom_title' => 'Recipe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        // dd("hii hardik");
        // dd($recipe);
        if (!empty($request->action) && $request->action == 'change_status') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            if ($recipe) {
                $recipe->is_active = $request->value;
                if ($recipe->save()) {
                    DB::commit();
                    $content['status'] = 200;
                    $content['message'] = "Status updated successfully.";
                }
            }
            return response()->json($content);
        }

        if ($request->form_step == "step_one") {
            try {
                DB::beginTransaction();
                $is_update =  $recipe->update($request->all());
                flash("recipe is updated successfully")->success();
                DB::commit();

                $path = storage_path('app/public/recipe_img/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                if (!empty($request->recipe_img)) {
                    foreach ($request->recipe_img as $key => $value) {
                        $file_name = time() . mt_rand(1, 99999) . "." . $value->getClientOriginalExtension();
                        $is_moved = $value->move($path, $path . $file_name);
                        if ($is_moved) {
                            Image::create([
                                "name" => $file_name,
                                "recipe_id" => $recipe->id,
                            ]);
                        } else {
                            continue;
                        }
                    }
                }

                return redirect()->route("admin.recipe.edit", ["recipe" => $recipe->custom_id])->with("form_step_data", "step_two_form");
            } catch (QueryException $e) {
                DB::rollback();
                flash($e->getMessage())->error();
                return redirect()->back();
            } catch (Exception $e) {
                flash($e->getMessage())->error();
                return redirect()->back();
            }
        }

        if ($request->form_step == "step_two") {
            // dd($recipe->id);
            try {
                $is_delete = RecipeIngredients::where("recipe_id", $recipe->id)->delete();

                $order = 0;
                foreach ($request->recipe_quantity_data as $key => $value) {
                    $order++;
                    RecipeIngredients::create([
                        "order" => $order,
                        "recipe_id" => $recipe->id,
                        "quantity_id" => $value ??  "0",
                        "measurment_id" => $request->recipe_measurment_data[$key] ?? "0",
                        "ingredients_id" => $request->ingredients_name[$key] ?? "0",
                    ]);
                }
                flash('recipe ingredient added successfully!')->success();
            } catch (Exception $e) {
                return redirect()->back()->with('error', $e->getMessage());
            }

            return redirect()->route("admin.recipe.edit", ["recipe" => $recipe->custom_id])->with("form_step_data", "step_three_form");
        }

        if ($request->form_step == "step_three") {

            // dd($request->input());

            try {
                $is_delete = RecipeDirection::where("recipe_id", $recipe->id)->delete();
                $order = 0;
                foreach ($request->title as $key => $value) {
                    // dd($value);
                    $order++;
                    RecipeDirection::create([
                        "order" => $order,
                        "recipe_id" => $recipe->id,
                        "title" => $value ?? "",
                        "description" => $request->description[$key] ?? "0",
                    ]);
                }
                flash('recipe direction updated successfully!')->success();
            } catch (QueryException $e) {
                flash($e->getMessage())->error();
                return redirect()->back();
            } catch (Exception $e) {

                flash($e->getMessage())->error();

                return redirect()->back()->with('error', $e->getMessage());
            }

            return redirect()->route("admin.recipe.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // dd($request->input());
        // dd($id);
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            // $all_recipe_id = ;

            foreach (explode(',', $request->ids) as $key => $value) {
                $recipe_id = Recipe::where("custom_id", $value)->first();
                $all_recipe_id[] = $recipe_id->id;
            }
            $recipe_images_all = Image::whereIn('recipe_id', array_values($all_recipe_id))->pluck('name')->toArray();
            $path = storage_path('app/public/recipe_img/');


            foreach ($recipe_images_all as $image) {
                // dd($image);
                if (!empty($image)) {
                    $is_delete = Storage::delete("recipe_img/" . $image);
                }
            }
            // Image::whereIn('recipe_id', array_values($all_recipe_id))->delete();

            $content['status'] = 200;
            $content['message'] = "recipe deleted successfully.";
            $content['count'] = Recipe::all()->count();
            return response()->json($content);
        } else {

            $recipe_id = Recipe::where("custom_id", $id)->first();
            $recipe_images_all = Image::where('recipe_id', $recipe_id->id)->get();
            if ($recipe_images_all->isNotEmpty()) {
                foreach ($recipe_images_all as $image) {
                    if (!empty($image)) {
                        $is_delete = Storage::delete("recipe_img/" . $image->name);
                    }
                }

                $recipe_id->delete();
                if (request()->ajax()) {
                    $content = array('status' => 200, 'message' => "recipe deleted successfully.", 'count' => Recipe::all()->count());
                    return response()->json($content);
                } else {
                    flash('recipe deleted successfully.')->success();
                    return redirect()->route('admin.recipe.index');
                }
            }
        }
    }
}
