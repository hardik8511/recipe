<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubAdminRequest;
use App\Mail\sendSubAdminPassword;
use App\Mail\sendUpdateProfileMail;
use App\Mail\subAdminWelcomeMail;
use App\Models\cr;
use App\Models\PasswordReset;
use App\Models\SubAdmin;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\TryCatch;

class SubAdminController extends Controller
{
    use SendsPasswordResetEmails;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("hii hardik");
        return view('admin.pages.subadmin.index')->with(['custom_title' => 'Sub Admin']);
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $records = [];
        $users = SubAdmin::orderBy($sort_column, $sort_order);

        if ($search != '') {
            $users->where(function ($query) use ($search) {
                $query->where('full_name', 'like', "%{$search}%")
                    ->orWhere('contact_no', 'like', "%{$search}%")
                    ->orWhere('email', 'like', "%{$search}%");
            });
        }

        $count = $users->count();
        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $users = $users->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $users = $users->get();
        foreach ($users as $user) {
            $params = [
                'checked' => ($user->is_active == 'y' ? 'checked' : ''),
                'getaction' => $user->is_active,
                'class' => '',
                'id' => $user->custom_id,
            ];

            $records['data'][] = [
                'id' => $user->id,
                'full_name' => $user->full_name,
                'email' => '<a href="mailto:' . $user->email . '" >' . $user->email . '</a>',
                'contact_no' => $user->contact_no ? '<a href="tel:' . $user->contact_no . '" >' . $user->contact_no . '</a>' : 'N/A',
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'User', 'id' => $user->custom_id], $user)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $user->custom_id)->render(),
            ];
        }
        // dd($records);
        return $records;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.subadmin.create')->with(['custom_title' => 'Sub Admin']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubAdminRequest $request)
    {
        //
        $path = NULL;
        if ($request->has('profile_photo')) {
            $path = $request->file('profile_photo')->store('subadmin/profile_photo');
        }
        // dd(auth()->user()->id);
        $user = SubAdmin::create([
            "full_name" => $request->full_name,
            "email" => $request->email,
            "admin_id" => auth()->user()->id ?? null,
            "contact_no" => $request->contact_no,
            "custom_id" => getUniqueString('sub_admins'),
            "password" => Hash::make("subadmin@123")
        ]);

        $user->profile_photo = $path;
        if ($user->save()) {
            //send mail to subadmin
            // dd($user->email);
            $token = generatePasswordResetToken($user->email);
            $is_send =  Mail::to($user->email)->send(new sendSubAdminPassword($user, $token));

            //send the welcome mail 
            $is_sub_admin_send = Mail::to($user->email)->send(new subAdminWelcomeMail($user));

            flash('Subadmin Account Created Successfully!')->success();
        } else {
            flash('Unable To Save Avatar. Please Try Again Later.')->error();
        }
        return redirect(route('admin.subadmin.index'));
    }


    public function resetSubAdminPassword(Request $request, $token = null)
    {
        // dd($request->input());
        // return view('mail.subadmin.subadmin_view') ;
        return view('mail.subadmin.subadmin_view')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    // public function showResetForm(Request $request, $token = null)
    // {
    //     return view('admin.auth.passwords.reset')->with(
    //         ['token' => $token, 'email' => $request->email]
    //     );
    // }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit(SubAdmin $subadmin)
    {
        return view('admin.pages.subadmin.edit', compact('subadmin'))->with(['custom_title' => 'Sub Admin']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function update(SubAdminRequest $request, SubAdmin $subadmin)
    {

        // dd("hii".$subadmin);

        try {
            DB::beginTransaction();
            if (!empty($request->action) && $request->action == 'change_status') {
                $content = ['status' => 204, 'message' => "Something Went Wrong"];
                if ($subadmin) {
                    $subadmin->is_active = $request->value;
                    if ($subadmin->save()) {
                        DB::commit();
                        $content['status'] = 200;
                        $content['message'] = "Status Updated Successfully.";
                    }
                }
                return response()->json($content);
            } else {
                $path = $subadmin->profile_photo;
                //request has remove_profie_photo then delete user image
                if ($request->has('remove_profie_photo')) {
                    if ($subadmin->profile_photo) {
                        Storage::delete($subadmin->profile_photo);
                    }
                    $path = null;
                }

                if ($request->hasFile('profile_photo')) {
                    if ($subadmin->profile_photo) {
                        Storage::delete($subadmin->profile_photo);
                    }
                    $path = $request->profile_photo->store('subadmin/profile_photo');
                }
                $subadmin->fill($request->all());
                $subadmin->profile_photo = $path;
                if ($subadmin->save()) {
                    DB::commit();
                    $this->sendUpdateProfileMail($subadmin);
                    flash('Subadmin Details Updated Successfully!')->success();
                } else {
                    flash('Unable To Update Subadmin. Try Again Later')->error();
                }
                return redirect(route('admin.subadmin.index'));
            }
        } catch (QueryException $e) {
            DB::rollback();
            // dd($e->getMessage());
            return redirect()->back()->flash('error', $e->getMessage());
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // dd($request->input());
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];

            $users_profile_photos = SubAdmin::whereIn('custom_id', explode(',', $request->ids))->pluck('profile_photo')->toArray();
            foreach ($users_profile_photos as $image) {
                if (!empty($image)) {
                    Storage::delete($image);
                }
            }
            SubAdmin::whereIn('custom_id', explode(',', $request->ids))->delete();
            $content['status'] = 200;
            $content['message'] = "User deleted successfully.";
            $content['count'] = SubAdmin::all()->count();
            return response()->json($content);
        } else {
            $user = SubAdmin::where('custom_id', $id)->firstOrFail();
            if ($user->profile_photo) {
                Storage::delete($user->profile_photo);
            }
            $user->delete();
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "User deleted successfully.", 'count' => SubAdmin::all()->count());
                return response()->json($content);
            } else {
                flash('User deleted successfully.')->success();
                return redirect()->route('admin.users.index');
            }
        }
    }

    public function sendUpdateProfileMail($user)
    {
        if ($user) {
            $is_send =  Mail::to($user->email)->send(new sendUpdateProfileMail($user));
        } else {
            return false;
        }
    }
}
