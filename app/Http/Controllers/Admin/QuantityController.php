<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuantityRequest;
use App\Models\Admin\Quantity;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuantityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.pages.quantity.index')->with(['custom_title' => 'Quantity']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $records = [];
        $quantities = Quantity::orderBy($sort_column, $sort_order);

        if ($search != '') {
            $quantities->where(function ($query) use ($search) {
                $query->where('no', 'like', "%{$search}%");
            });
        }

        $count = $quantities->count();
        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $quantities = $quantities->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $quantities = $quantities->get();
        foreach ($quantities as $quantity) {
            // dd($quantity);
            $params = [
                'checked' => ($quantity->is_active == 'y' ? 'checked' : ''),
                'getaction' => $quantity->is_active,
                'class' => '',
                'id' => $quantity->custom_id,
            ];

            $records['data'][] = [
                'id' => $quantity->id,
                'no' => $quantity->no ?? "0",
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'Quantity', 'id' => $quantity->custom_id], $quantity)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $quantity->custom_id)->render(),
            ];
        }
        // dd($records);
        return $records;
    }

    //
    public function create()
    {
        return view('admin.pages.quantity.create')->with(['custom_title' => 'Quantity']);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuantityRequest $request)
    {
        $request['custom_id']   =   getUniqueString('quantity');
        $quantity = Quantity::create($request->all());
        if ($quantity->save()) {
            flash('quantity created successfully!')->success();
        } else {
            flash('Unable to save quantity. Please try again later.')->error();
        }
        return redirect(route('admin.quantity.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Quantity $quantity)
    {
        return view('admin.pages.quantity.edit', compact('quantity'))->with(['custom_title' => 'Quantity']);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuantityRequest $request, Quantity $quantity)
    {
        try {
            DB::beginTransaction();
            if (!empty($request->action) && $request->action == 'change_status') {
                $content = ['status' => 204, 'message' => "something went wrong"];
                if ($quantity) {
                    $quantity->is_active = $request->value;
                    if ($quantity->save()) {
                        DB::commit();
                        $content['status'] = 200;
                        $content['message'] = "Status updated successfully.";
                    }
                }
                return response()->json($content);
            } else {
                $quantity->fill($request->all());
                if ($quantity->save()) {
                    DB::commit();
                    flash('quantity details updated successfully!')->success();
                } else {
                    flash('Unable to updatate quantity. Try again later')->error();
                }
                return redirect(route('admin.quantity.index'));
            }
        } catch (QueryException $e) {
            DB::rollback();
            flash('error', $e->getMessage());
            return redirect()->back();
        } catch (Exception $e) {
            flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            Quantity::whereIn('custom_id', explode(',', $request->ids))->delete();
            $content['status'] = 200;
            $content['message'] = "quantity deleted successfully.";
            $content['count'] = Quantity::all()->count();
            return response()->json($content);
        } else {
            $ingredient = Quantity::where('custom_id', $id)->firstOrFail();
            $ingredient->delete();
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "quantity deleted successfully.", 'count' => Quantity::all()->count());
                return response()->json($content);
            } else {
                flash('quantity deleted successfully.')->success();
                return redirect()->route('admin.quantity.index');
            }
        }
    }
}
