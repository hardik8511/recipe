<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DropzoneController extends Controller
{
    //
    public function index()
    {
        return view("admin.pages.dropzone.index");
    }
    public function dropzoneFileUpload(Request $request)
    {
        // dd($request->_token);
        // dd($request->input());
        if ($request->recipe_id) {
            $image = $request->file('file');
            // dd($image);
            $img_name = $image->getClientOriginalName();
            // dd($img_name);
            $recipe = Image::create([
                "name" => $img_name,
                "recipe_id" => $request->recipe_id ?? "0",
            ]);
            if ($recipe) {
                $path = $image->storeAs('recipe_img/', $img_name);
            }


            return response()->json(['success' => $image->getClientOriginalName()]);
        } else {
            $image = $request->file('file');
            $img_name = $image->getClientOriginalName();
            $path = $image->storeAs('tmp_dropzone/' . $request->_token . "/", $img_name);
            return response()->json(['success' => $image->getClientOriginalName()]);
        }
    }

    public function recipeImageDelete(Request $request)
    {
        // dd($request->input());
        // dd($request->input());
        // dd($request->all());
        // $is_delete = Storage::delete('tmp_dropzone/' . $request->_token . "/".$request->filename);
        // dd($is_delete);
        // if ($request->type == "local") {
        //     if (Storage::exists('tmp_dropzone/' . $request->tmp_token . "/" . $request->filename)) {
        //         $is_delete = Storage::delete('tmp_dropzone/' . $request->tmp_token . "/" . $request->filename);
        //         return $is_delete;
        //     } else {
        //         echo "File does not exist";
        //         return false;
        //     }
        // }
        // if ($request->type == "edit_mode") {
        //     // dd(Storage::exists('recipe_img/'. $request->filename));

        //     $delete_recipe_data = Image::where("name", $request->filename)->delete();
        //     if (Storage::exists('recipe_img/' . $request->filename)) {
        //         // dd($request->filename);
        //         // dd($delete_recipe_data);
        //         $is_delete = Storage::delete('recipe_img/' . $request->filename);
        //         return $is_delete;
        //     } else {
        //         echo "File does not exist";
        //         return false;
        //     }
        // } else {
        //     return false;
        // }

        //code after custom create file upload control
        if ($request->recipe_img_name) {

            //delete the image from storage 
            $is_delete_file =   Storage::delete('recipe_img/' . $request->recipe_img_name);
            // also delete the entry from database 
            $is_delete_data = Image::where([["name", $request->recipe_img_name], ["recipe_id", $request->recipe_id]])->delete();
            if ($is_delete_data) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function fetchDropzoneImage(Request $request)
    {
        // dd("hiii");
        // dd($request->input());

        if ($request->type = "edit_recipe_img") {
            $output = "<div class='row'> ";
            $files = [];
            // $recipe_id 
            // DB::enableQueryLog();
            $files = Image::where("recipe_id", $request->recipe_id)->get();
            foreach ($files as $key => $value) {
                // dd($value);
                $output .= '
                <div class="dz-preview dz-complete dz-image-preview">
                        <div class="dz-image"> 
                             <img data-dz-thumbnail src="' . url("storage/recipe_img/" . $value->name) . '" class="img-thumbnail"  height="100px" width="auto"/> 
                        </div> 
                             <a class="dz-remove remove_uploaded_files" data-dz-remove data-id="' . $value->name . '" href="javascript:undefined;" data-dz-remove="">Remove file</a>
                             </div>';
            }
            $output .= "</div>";
        } else {
            $files = Storage::disk("public")->files("tmp_dropzone/" . $request->_token . "/");
            // dd($files);
            $output = "<div class='row'> ";
            foreach ($files as $key => $value) {
                // dd($value);

                $file_name = substr(strrchr($value, '/'), 1);

                $output .= '
                <div class="dz-preview dz-complete dz-image-preview">
                        <div class="dz-image"> 
                             <img data-dz-thumbnail src="' . url("storage/" . $value) . '" class="img-thumbnail"  height="100px" width="auto"/> 
                        </div> 
                             <a class="dz-remove remove_uploaded_files" data-dz-remove data-id="' . $file_name . '" href="javascript:undefined;" data-dz-remove="">Remove file</a>
                             </div>';
            }
            $output .= "</div>";
        }
        // dd($output);
        echo $output;
    }
}
