<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Favorite;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class FavoriteController extends Controller
{
    //
    public function favorite(Request $request)
    {
        $status = "";
        try {
            $user_id = auth()->user()->id;
            $is_already_favorite =   Favorite::where([['admin_id', '=', $user_id], ['recipe_id', $request->recipe_id]])->first();
            if ($is_already_favorite) {
                $status = "remove_from_favorite";
                $is_already_favorite->delete();
            } else {
                $request["admin_id"] = $user_id;
                $is_create =  Favorite::create($request->all());
                $status = "added_to_favorite";
            }
        } catch (Exception $ex) {
            $status = "someting_wrong" . $ex->getMessage();
        }

        return $status;
    }
}
