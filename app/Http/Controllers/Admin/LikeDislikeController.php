<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LikeDislike;
use Exception;
use Illuminate\Http\Request;

class LikeDislikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function likeDislike(Request $request)
    {
        // dd($request->input());
        //1. not like or dislike
        //2. already like 
        //3. already dislike 
        $status = "";
        $user_id = auth()->user()->id;

        //checking the recipe is found or not 
        $is_found = LikeDislike::where([['admin_id', '=', $user_id], ['recipe_id', $request->recipe_id]])->first();
        if ($is_found) {
            if ($is_found->is_status == "like" && $request->is_status == "like") {
                $status = "already_liked";
            } else if ($is_found->is_status == "dislike" && $request->is_status == "dislike") {
                $status = "already_disliked";
            } elseif ($is_found->is_status == "like" && $request->is_status == "dislike") {
                $status = "recipe_dislike_sucessfully";
                $is_found->is_status = "dislike";
                $is_found->admin_id = $user_id;
                $is_create = $is_found->save();
            } elseif ($is_found->is_status == "dislike" && $request->is_status == "like") {
                $status = "recipe_like_sucessfully";
                $is_found->is_status = "like";
                $is_found->admin_id = $user_id;
                $is_create = $is_found->save();
            }
        } else {
            if ($request->is_status == "like") {
                $status = "recipe_like_sucessfully";
                $request['is_status'] = "like";
                $request['admin_id'] = $user_id;
                $is_create = LikeDislike::create($request->all());
            }
            if ($request->is_status == "dislike") {
                $status = "recipe_dislike_sucessfully";
                $request['is_status'] = "dislike";
                $request['admin_id'] = $user_id;
                $is_create = LikeDislike::create($request->all());
            }
        }
        $total_like = LikeDislike::where([["recipe_id", $request->recipe_id], ["is_status", "like"]])->count();
        $total_dislike = LikeDislike::where([["recipe_id", $request->recipe_id], ["is_status", "dislike"]])->count();

        $status = [
            "status" => $status,
            "like_count" =>  $total_like,
            "dislike_count" => $total_dislike,
        ];

        return response()->json($status);
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
