<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MeasurmentRequest;
use App\Models\Admin\MeasurMent;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MeasureMentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.pages.measurment.index')->with(['custom_title' => 'Measurment']);
    }
    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $records = [];
        $measurement = MeasurMent::orderBy($sort_column, $sort_order);

        if ($search != '') {
            $measurement->where(function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%");
            });
        }

        $count = $measurement->count();
        $records['recordsTotal'] = $count;
        $records['recordsFiltered'] = $count;
        $records['data'] = [];

        $measurement = $measurement->offset($offset)->limit($limit)->orderBy($sort_column, $sort_order);

        $measurement = $measurement->get();
        foreach ($measurement as $measure) {
            // dd($measure);
            $params = [
                'checked' => ($measure->is_active == 'y' ? 'checked' : ''),
                'getaction' => $measure->is_active,
                'class' => '',
                'id' => $measure->custom_id,
            ];

            $records['data'][] = [
                'id' => $measure->id,
                'name' => $measure->name ?? "",
                'active' => view('admin.layouts.includes.switch', compact('params'))->render(),
                'action' => view('admin.layouts.includes.actions')->with(['custom_title' => 'measure', 'id' => $measure->custom_id], $measure)->render(),
                'checkbox' => view('admin.layouts.includes.checkbox')->with('id', $measure->custom_id)->render(),
            ];
        }
        // dd($records);
        return $records;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.measurment.create')->with(['custom_title' => 'Measurement']);

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeasurmentRequest $request)
    {
        $request['custom_id']   =   getUniqueString('measurment');
        $measurement = MeasurMent::create($request->all());
        if ($measurement->save()) {
            flash('measurment created successfully!')->success();
        } else {
            flash('Unable to save measurement. Please try again later.')->error();
        }
        return redirect(route('admin.measurement.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MeasurMent $measurement)
    {
        //
        // dd($measurement);
        return view('admin.pages.measurment.edit', compact('measurement'))->with(['custom_title' => 'Measurement']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MeasurmentRequest $request, MeasurMent $measurement)
    {
        try {
            DB::beginTransaction();
            if (!empty($request->action) && $request->action == 'change_status') {
                $content = ['status' => 204, 'message' => "something went wrong"];
                if ($measurement) {
                    $measurement->is_active = $request->value;
                    if ($measurement->save()) {
                        DB::commit();
                        $content['status'] = 200;
                        $content['message'] = "Status updated successfully.";
                    }
                }
                return response()->json($content);
            } else {
                $measurement->fill($request->all());
                if ($measurement->save()) {
                    DB::commit();
                    flash('measurement details updated successfully!')->success();
                } else {
                    flash('Unable to update user. Try again later')->error();
                }
                return redirect(route('admin.measurement.index'));
            }
        } catch (QueryException $e) {
            DB::rollback();
            // dd("hii");
            flash('error', $e->getMessage());
            return redirect()->back();
        } catch (Exception $e) {
            // dd($e->getMessage());
            flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            MeasurMent::whereIn('custom_id', explode(',', $request->ids))->delete();
            $content['status'] = 200;
            $content['message'] = "measurement deleted successfully.";
            $content['count'] = MeasurMent::all()->count();
            return response()->json($content);
        } else {
            $ingredient = MeasurMent::where('custom_id', $id)->firstOrFail();
            $ingredient->delete();
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "measurement deleted successfully.", 'count' => MeasurMent::all()->count());
                return response()->json($content);
            } else {
                flash('measurement deleted successfully.')->success();
                return redirect()->route('admin.measurement.index');
            }
        }
    }
}
