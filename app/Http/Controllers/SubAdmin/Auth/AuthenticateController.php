<?php

namespace App\Http\Controllers\SubAdmin\Auth;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\SubAdmin;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset as ResetPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthenticateController extends Controller
{
    public function showResetPassword($token)
    {
        // dd($token);
        return view("subadmin.auth.password_reset", ["token" => $token]);
    }

    //old method to reset the password 
    public function resetPassword(Request $request)
    {
        // dd("hii");
        $reset_pasword_found = PasswordReset::where([
            ["email", $request->email],
            ["token", $request->password_token]
        ])->first();
        // dd($reset_pasword_found);
        if ($reset_pasword_found) {
            try {
                $sub_admin_data = SubAdmin::where("email", $reset_pasword_found->email)->update([
                    'password' => Hash::make($request->password)
                ]);

                $delete_records = PasswordReset::where([
                    ["email", $request->email],
                    ["token", $request->password_token]
                ])->delete();

                return redirect()->back()->with(["success" => "password update successfully"]);
            } catch (Exception $th) {
                return redirect()->back()->with(["danger" => "someting is wrong" . $th->getMessage()]);
            }
        } else {
            return redirect()->back()->with(["danger" => "someting is wrong or already reset the password"]);
        }
    }

    public function showLogin()
    {
        return view("subadmin.auth.login");
    }

    public function checkLogin(Request $request)
    {
        // // dd($request->input());
        // $email = $request->email;
        // $password = $request->password;
        $credential = [
            "email" => $request->email,
            "password" => $request->password,
        ];
        $user_data = Auth::guard("admin")->attempt($credential);

        if ($user_data) {
            $user = Auth::guard("admin")->user();
            return redirect()->route("admin.dashboard.index");
        }
        // $is_login = Auth::attempt($credential);
        // dd($is_login);
    }
    public function showForgotPassword()
    {
        return view('subadmin.auth.forgot_password');
    }

    public function sendLinkForgotPassword(Request $request)
    {
        // dd($request->input());
        $request->validate(['email' => 'required|email']);
        // dd("hiii");
        $status = Password::broker("admin")->sendResetLink(
            $request->only('email')
        );

        dd($status);

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);
    }
    public function showResetForm($token)
    {
        return view('subadmin.auth.password_reset', ["token" => $token]);
    }

    public function reset(Request $request)
    {
        // dd($request->input());
        // dd("hii");
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
        // dd("hiii");
        $status = Password::broker('sub_admins')->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
                // ]);
                // dd($user);
                $user->save();

                event(new ResetPassword($user));
            }
        );

        return $status === Password::PASSWORD_RESET
            ? redirect()->route('sub_admin.login')->with('status', __($status))
            : back()->withErrors(['email' => [__($status)]]);
    }

    //register the subadmin route 
    public function registration()
    {
        return view("subadmin.auth.register");
    }
    public function postRegistration(Request $request)
    {
        // dd($request->all());
        $rules = [
            "full_name" => 'required|max:60',
            "email" => 'required|unique:sub_admins,email|email|',
            "contact_no" => 'required|unique:sub_admins,contact_no',
            "password" => 'required|min:8|max:60',
        ];
        $validate = Validator::make($request->input(), $rules);
        if ($validate->fails()) {

            return redirect()->route("sub_admin.registration")->with(["danger" => $validate->errors()->first()]);
        }

        try {
            // $path = NULL;
            // if ($request->has('profile_photo')) {
            //     $path = $request->file('profile_photo')->store('subadmin/profile_photo');
            // }
            // $user = SubAdmin::create([
            //     "full_name" => $request->full_name,
            //     "email" => $request->email,
            //     "admin_id" => auth()->user()->id ?? "0",
            //     "contact_no" => $request->contact_no,
            //     "custom_id" => getUniqueString('sub_admins'),
            //     "password" => Hash::make($request->password),
            // ]);

            // dd(getPermissions('sub_admin'));
            $permission = serialize(getPermissions('sub_admin'));;

            $user = Admin::create([
                "full_name" => $request->full_name,
                "email" => $request->email,
                "contact_no" => $request->contact_no,
                "password" => Hash::make($request->password),
                "permissions" => $permission,
                "type" => "role"
            ]);
            // $user->profile_photo = $path;
            if ($user->save()) {

                // dd("registraiton successfully ");
                return redirect()->route("sub_admin.login")->with(["success" => "register successfully please login"]);
                flash('subadmin account created successfully!')->success();
            } else {
                flash('Unable to save avatar. Please try again later.')->error();
            }
        } catch (\Exception $e) {
            return redirect()->route("sub_admin.registration")->with(["danger" => $e->getMessage()]);
        }
    }
}
