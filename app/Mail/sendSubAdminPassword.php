<?php

namespace App\Mail;

use App\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class sendSubAdminPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user, $token;
    public function __construct($user, $token)
    {
        $this->user  = $user;
        $this->token  = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject("Reset Your Password")->view('mail.subadmin.sendpassword', ['user_data' => $this->user, "token" => $this->token]);
    }
}
