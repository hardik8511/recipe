<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    public function getRouteKeyName()
    {
        return 'custom_id';
    }

    protected $table = "ingredients";
    protected $fillable = ["custom_id","name"];
    use HasFactory;
}
