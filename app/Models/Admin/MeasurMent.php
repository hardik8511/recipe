<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeasurMent extends Model
{
    public function getRouteKeyName()
    {
        return 'custom_id';
    }
    protected $table = "measurment";
    protected $fillable = ['name','custom_id']; 
    use HasFactory;
}
