<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    public function getRouteKeyName()
    {
        return 'custom_id';
    }
    protected $table = "quantity";
    protected $fillable = ["no", "custom_id"];
    use HasFactory;
}
