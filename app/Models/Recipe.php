<?php

namespace App\Models;

use App\Models\Admin\Ingredients;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public function getRouteKeyName()
    {
        return 'custom_id';
    }

    protected $table = "recipes";
    protected $fillable = ["custom_id", "admin_id", "sub_category_id", "title", "preperation_time", "cooking_time", "total_time", "is_featured", "servings", "fat", "sodium", "carbohydrates", "sugar", "protein", "calories", "fiber", "is_active", 'description'];
    use HasFactory;

    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(Image::class, "recipe_id", "id");
    }

    public function favorites()
    {
        return $this->hasOne(Favorite::class, 'recipe_id', 'id')->where('admin_id', '=', auth()->user()->id);
    }

    public function likedislike()
    {
        return $this->hasOne(LikeDislike::class, 'recipe_id', 'id')->where('admin_id', '=', auth()->user()->id);
    }

    //get all images uploaded recipe 
    public function images()
    {
        return $this->hasMany(Image::class, 'recipe_id', "id");
    }

    //recipe ingredients 
    public function ingredients()
    {
        return $this->hasMany(RecipeIngredients::class, "recipe_id", "id");
    }

    // recipe direction 
    public function directions()
    {
        return $this->hasMany(RecipeDirection::class, "recipe_id", "id");
    }


    // public function quantities()
    // {
    //     return $this->hasMany(RecipeIngredients::class, "recipe_id", "id");
    // }
    // public function measurments()
    // {
    //     return $this->hasMany(RecipeIngredients::class, "recipe_id", "id");
    // }


    // public function quantity()
    // {
    //     return $this->hasOneThrough(Ingredients::class, RecipeIngredients::class);
    // }

    // public function measurment()
    // {
    //     return $this->hasOneThrough(Ingredients::class, RecipeIngredients::class);
    // }

    public function likes()
    {
        return $this->hasMany(LikeDislike::class, "recipe_id", "id")->where("is_status", "like");
    }
    public function dislikes()
    {
        return $this->hasMany(LikeDislike::class, "recipe_id", "id")->where("is_status", "dislike");
    }
}
