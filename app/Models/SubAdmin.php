<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Notifications\SubAdminResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\User as Authenticatable;
// class SubAdmin extends Authenticatable  implements CanResetPasswordContract
// {
class SubAdmin extends Authenticatable implements CanResetPasswordContract
{
    use HasFactory,Notifiable,CanResetPassword;
    protected $fillable=["full_name", "email", "contact_no", "password", "is_active", "profile_photo","custom_id","admin_id"];
    public function getRouteKeyName()
    {
        return 'custom_id';
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SubAdminResetPassword($token));
    }
}
