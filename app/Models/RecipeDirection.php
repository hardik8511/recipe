<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeDirection extends Model
{
    use HasFactory;
    protected $table = "recipe_directions";
    protected $fillable = ["recipe_id", "order", "title", "description"];

}
