<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public function getRouteKeyName()
    {
        return 'custom_id';
    }

    public function subcategories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }
}
