<?php

namespace App\Models;

use App\Models\Admin\Ingredients;
use App\Models\Admin\MeasurMent;
use App\Models\Admin\Quantity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeIngredients extends Model
{
    use HasFactory;
    protected $fillable = ["recipe_id", "order", "quantity_id", "measurment_id", "ingredients_id"];
    protected $table = "recipe_ingredients";

    public function ingredient()
    {
        return $this->belongsTo(Ingredients::class, "ingredients_id", "id");
    }
    public function measurment()
    {
        return $this->belongsTo(MeasurMent::class, "measurment_id", "id");
    }

    public function quantity()
    {
        return $this->belongsTo(Quantity::class, "quantity_id", "id");
    }
}
