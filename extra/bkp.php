<form id="frmAddRecipe" method="POST" action="{{ route('admin.recipe.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        {{-- First Name --}}
        <div class="form-group">
            <label for="title">{!! $mend_sign !!} Title : </label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                name="title" value="{{ old('title') }}" placeholder="Enter Recipe Name" autocomplete="title"
                spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('title'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>

        {{-- preparation time : preparation_time --}}
        <div class="form-group">
            <label for="preparation_time">{!! $mend_sign !!} Preparation Time : </label>
            <input type="text" class="form-control @error('preparation_time') is-invalid @enderror"
                id="preparation_time" name="preparation_time" value="{{ old('preparation_time') }}"
                placeholder="Enter Recipe Name" autocomplete="preparation_time" spellcheck="false"
                autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('preparation_time'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('preparation_time') }}</strong>
                </span>
            @endif
        </div>

        {{-- cooking time : cooking_time --}}
        <div class="form-group">
            <label for="cooking_time">{!! $mend_sign !!} Preparation Time : </label>
            <input type="text" class="form-control @error('cooking_time') is-invalid @enderror"
                id="cooking_time" name="cooking_time" value="{{ old('cooking_time') }}"
                placeholder="Enter Recipe Name" autocomplete="cooking_time" spellcheck="false"
                autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('cooking_time'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('cooking_time') }}</strong>
                </span>
            @endif
        </div>

        {{-- servings  : servings --}}
        <div class="form-group">
            <label for="servings">{!! $mend_sign !!} Servings : </label>
            <input type="number" class="form-control @error('servings') is-invalid @enderror" id="servings"
                name="servings" value="{{ old('servings') }}" placeholder="Enter Recipe Name"
                autocomplete="servings" spellcheck="false" autocapitalize="sentences" tabindex="0"
                autofocus />
            @if ($errors->has('servings'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('servings') }}</strong>
                </span>
            @endif
        </div>

        {{-- carbohydrates  : fat --}}
        <div class="form-group">
            <label for="fat">{!! $mend_sign !!} Fat : </label>
            <input type="text" class="form-control @error('fat') is-invalid @enderror" id="fat"
                name="fat" value="{{ old('fat') }}" placeholder="Enter Recipe Name" autocomplete="fat"
                spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('fat'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('fat') }}</strong>
                </span>
            @endif
        </div>

        {{-- sodium  : sodium --}}
        <div class="form-group">
            <label for="sodium">{!! $mend_sign !!} Sodium : </label>
            <input type="text" class="form-control @error('sodium') is-invalid @enderror" id="sodium"
                name="sodium" value="{{ old('sodium') }}" placeholder="Enter Recipe Name"
                autocomplete="sodium" spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('sodium'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('sodium') }}</strong>
                </span>
            @endif
        </div>

        {{-- carbohydrates  : carbohydrates --}}
        <div class="form-group">
            <label for="carbohydrates">{!! $mend_sign !!} Carbohydrates : </label>
            <input type="text" class="form-control @error('carbohydrates') is-invalid @enderror"
                id="carbohydrates" name="carbohydrates" value="{{ old('carbohydrates') }}"
                placeholder="Enter Recipe Name" autocomplete="carbohydrates" spellcheck="false"
                autocapitalize="sentences" tabindex="0" autofocus />
            @if ($errors->has('carbohydrates'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('carbohydrates') }}</strong>
                </span>
            @endif
        </div>

        {{-- calories  : sugar --}}
        <div class="form-group">
            <label for="sugar">{!! $mend_sign !!} sugar : </label>
            <input type="text" class="form-control @error('sugar') is-invalid @enderror" id="sugar"
                name="sugar" value="{{ old('sugar') }}" placeholder="Enter Recipe Name"
                autocomplete="sugar" spellcheck="false" autocapitalize="sentences" tabindex="0"
                autofocus />
            @if ($errors->has('sugar'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('sugar') }}</strong>
                </span>
            @endif
        </div>

        {{-- protein  : protein --}}
        <div class="form-group">
            <label for="protein">{!! $mend_sign !!} protein : </label>
            <input type="text" class="form-control @error('protein') is-invalid @enderror" id="protein"
                name="protein" value="{{ old('protein') }}" placeholder="Enter Recipe Name"
                autocomplete="protein" spellcheck="false" autocapitalize="sentences" tabindex="0"
                autofocus />
            @if ($errors->has('protein'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('protein') }}</strong>
                </span>
            @endif
        </div>

        {{-- calories  : calories --}}
        <div class="form-group">
            <label for="calories">{!! $mend_sign !!} calories : </label>
            <input type="text" class="form-control @error('calories') is-invalid @enderror" id="calories"
                name="calories" value="{{ old('calories') }}" placeholder="Enter Recipe Name"
                autocomplete="calories" spellcheck="false" autocapitalize="sentences" tabindex="0"
                autofocus />
            @if ($errors->has('calories'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('calories') }}</strong>
                </span>
            @endif
        </div>

        {{-- fiber  : fiber --}}
        <div class="form-group">
            <label for="fiber">{!! $mend_sign !!} fiber : </label>
            <input type="text" class="form-control @error('fiber') is-invalid @enderror" id="fiber"
                name="fiber" value="{{ old('fiber') }}" placeholder="Enter Recipe Name"
                autocomplete="fiber" spellcheck="false" autocapitalize="sentences" tabindex="0"
                autofocus />
            @if ($errors->has('fiber'))
                <span class="help-block">
                    <strong class="form-text">{{ $errors->first('fiber') }}</strong>
                </span>
            @endif
        </div>

        {{-- start recipe ingredient module --}}
        <div class="append_html_ingredient">
            <div class="form-group">
                <label for="">Recipe Ingredient : </label>
                <div class="row ">
                    <div class="col-md-3">
                        <select name="recipe_quantity_data[]"
                            class="form-control custom_select_two_class required" id="">
                            <option value="">select quantity </option>
                            @foreach ($recipe_quantity_data as $recipe_quantity)
                                <option value="{{ $recipe_quantity->id }}"> {{ $recipe_quantity->no }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select name="recipe_measurment_data[]"
                            class="form-control custom_select_two_class required" id="">
                            <option value=""> select measurment </option>
                            @foreach ($recipe_measurment_data as $recipe_measurment)
                                <option value="{{ $recipe_measurment->id }}"> {{ $recipe_measurment->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select name="ingredients_name[]"
                            class="form-control custom_select_two_class required" id="">
                            <option value="">select ingredient</option>
                            @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                <option value="{{ $recipe_ingredient->id }}"> {{ $recipe_ingredient->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <button class="btn btn-primary" id="add_more_btn"> Add More </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="append_tag" id="appended_tags">
        </div>

    </div>
</form>





=========
<div class="container mt-2 dz-default dz-message">
    <div class="row">
        <div class="col-md-12 dz-clickable">
            <form action="{{ route('admin.dropzoneFileUpload') }}" method="post"
                enctype="multipart/form-data" id="image-upload" class="dropzone mb-10"
                style="border-radius: 30px">
                @csrf
                <div style="" id="already_uploaded_img">
                    
                </div>
              
            </form>
        </div>
    </div>
</div>
=============


@push('extra-js-scripts')

 {{-- start the dropzone script  --}}
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var dropzone = new Dropzone(".dropzone", {
            maxFiles: 8,
            maxFilesize: 5,
            preventDuplicates: true,
            renameFile: function(file) {
                // console.log();
                var dt = new Date();
                var time = dt.getTime();
                var random_digit = Math.floor(Math.random() * 100000) + 1;
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
                // console.log(extension);
                return time + random_digit + "." + extension;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function(file) {
                var name = file.upload.filename;
                let _token = "{{ csrf_token() }}";
                
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: name,
                        type: "local",
                        tmp_token : _token,
                        
                    },
                    success: function(data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            success: function(file, response) {
                console.log(response);
            },
            error: function(file, response) {
                return false;
            }
        });

        //start  dropzone file remove 

        $(document).on('click', '.remove_uploaded_files', function(e) {
            var old_this = this;
            let fileName = $(this).data("id");
            if (fileName) {
                let _token = "{{ csrf_token() }}";
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: fileName,
                        type: "local",
                        tmp_token : _token,
                    },
                    success: function(data) {
                        $(old_this).closest(".dz-image-preview").remove();
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                     
            }
        });

        //end dropzone file remove 
        //start fetch uploaded images 
        
        uploaded_images();
        function uploaded_images() {
            $.ajax({
                type: "get",
                url: "{{route('admin.uploaded_img')}}",
                data: {
                    "_token" : "{{ csrf_token() }}",
                },
                
                success: function (response) {
                    $("#already_uploaded_img").html(response);
                }
            });
        }

        //end fetch uploaded images 
    </script>
    {{-- end the dorpzone script  --}}

    <script>
        $(document).ready(function() {
            //start add more functionality for recipe ingredient 
           
            //start select 2 
            // $(".custom_select_two_class").select2();
            //end select 2
            $("#frmAddRecipe").validate({
                rules: {
                    title: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                    },
                },
                messages: {
                    title: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Title', 'min' => 3])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            let start_btn = 0 ;
            $("#add_more_btn").click(function(e) {
                e.preventDefault();
                addMoreIngredient();
            });

            //start add more ingredient function  
            function addMoreIngredient(){
                //drag and drop 
                var el1 = document.getElementById("appended_tags");
                var sortable = Sortable.create(el1);

                start_btn++;

                let append_html = `<div class="form-group ingredient_remove_div" >
                            <div class="row ">
                                <div class="col-md-3">
                                    <select name="recipe_quantity_data[${start_btn}]"
                                        class=" form-control custom_select_two_class classToValidate"
                                        id="">
                                        <option value=""> select quantity </option>
                                        @foreach ($recipe_quantity_data as $recipe_quantity)
                                            <option value="{{ $recipe_quantity->id }}"> {{ $recipe_quantity->no }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <select name="recipe_measurment_data[${start_btn}]"
                                        class="form-control custom_select_two_class classToValidate" id="">
                                        <option value=""> select measurment </option>
                                        @foreach ($recipe_measurment_data as $recipe_measurment)
                                            <option value="{{ $recipe_measurment->id }}"> {{ $recipe_measurment->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <select name="ingredients_name[${start_btn}]"
                                        class="form-control custom_select_two_class classToValidate" id="">
                                        <option value="">select ingredient</option>
                                        @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                            <option value="{{ $recipe_ingredient->id }}"> {{ $recipe_ingredient->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <button class="btn btn-danger remove_btn_click" id="current_remove_html"> Remove
                                    </button>

                                </div>
                            </div>
                        </div>`;
                        
                        $("#appended_tags").append(append_html);
                        

                //adding the validation rules 
                $("[name='recipe_quantity_data[" + start_btn + "]']").rules("add", {
                    required: true,
                    not_empty: true,
                    messages: {
                        recipe_quantity_data: {
                        required: "@lang('validation.required', ['attribute' => 'quantity'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'quantity'])",
                     },
                    }
                });
                $("[name='recipe_measurment_data[" + start_btn + "]']").rules("add", {
                    required: true,
                    not_empty: true,
                    messages: {
                        recipe_measurment_data: {
                        required: "@lang('validation.required', ['attribute' => 'measurment'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'measurment'])",
                     },
                    }
                });
                $("[name='ingredients_name[" + start_btn + "]']").rules("add", {
                    required: true,
                    not_empty: true,
                    messages: {
                        recipe_measurment_data: {
                        required: "@lang('validation.required', ['attribute' => 'ingredient name'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'ingredient name'])",
                     },
                    }
                });
            }
            //end add more ingredient function 
            
            // function removeIngredient() {
            //     console.log(re);
            // }

            $(document).on('click', '.remove_btn_click', function(e) {
                e.preventDefault();
                $(this).closest(".ingredient_remove_div").remove();
            });

            $("#frm_submit_main").click(function(e) {
                $('#frmAddRecipe').submit();

            });
            $('#frmAddRecipe').submit(function() {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endpush
============