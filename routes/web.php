<?php

use App\Http\Controllers\Admin\SubAdminController;
use App\Http\Controllers\SubAdmin\Auth\AuthenticateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('index');
});


Auth::routes(['register' => false, 'login' => false]);

Route::get('login', 'AdminAuth\LoginController@showLoginForm')->name('login');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
  Route::get('login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
  Route::post('login', 'AdminAuth\LoginController@login');
  Route::get('logout', 'AdminAuth\LoginController@logout')->name('admin.logout');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin.password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
  Route::get('/password/reset/{token}/{email?}', 'AdminAuth\ResetPasswordController@showResetForm');
});
Route::post('check-email', 'UtilityController@checkEmail')->name('check.email');
Route::post('check-contact', 'UtilityController@checkContact')->name('check.contact');

//sub-admin routes
Route::group(['prefix' => 'sub-admin','as'=>'sub_admin.'], function () {
  
  //subadmin reset the password
  Route::get('reset-password/{token?}', [AuthenticateController::class, 'showResetPassword'])->name('reset_password_link');
  Route::post('reset-password', [AuthenticateController::class, 'resetPassword'])->name("reset-password");

  //login route 
  Route::get('login', [AuthenticateController::class, 'showLogin'])->name('login');
  Route::post('login', [AuthenticateController::class, 'checkLogin'])->name('checkLogin');
  
  //forgot password
  Route::get('forgot-password', [AuthenticateController::class, 'showForgotPassword'])->name('forgot_password');
  Route::post('send-link', [AuthenticateController::class, 'sendLinkForgotPassword'])->name('send_link');

  //this is the new method to reset the password using password broker 
  // Route::get('/password/reset/{token?}', [AuthenticateController::class, 'showResetForm'])->name('reset_password_form');
  // Route::post('/password/reset', [AuthenticateController::class, 'reset'])->name('reset_password'); 

  //sub admin register 


  //category and sub category crud 
  Route::get('/register', [AuthenticateController::class, 'registration'])->name('registration');
  Route::post('/register', [AuthenticateController::class, 'postRegistration'])->name('registration.post'); 

});

