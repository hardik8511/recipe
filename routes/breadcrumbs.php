<?php

use Illuminate\Support\Facades\Auth;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Dashboard ---------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
	$breadcrumbs->push('Dashboard', route(Auth::getDefaultDriver() . '.dashboard.index'));
});

// Users -------------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('users_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('Users', route(Auth::getDefaultDriver() . '.users.index'));
});

// Quick Links
Breadcrumbs::register('quick_link', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Mange Quick Link'), route('admin.quickLink'));
});
// Profile
Breadcrumbs::register('my_profile', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Manage Account'), route('admin.profile-view'));
});

Breadcrumbs::register('users_create', function ($breadcrumbs) {
	$breadcrumbs->parent('users_list');
	$breadcrumbs->push('Add New User', route(Auth::getDefaultDriver() . '.users.create'));
});

Breadcrumbs::register('users_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('users_list');
	$breadcrumbs->push('Edit User', route(Auth::getDefaultDriver() . '.users.edit', $id));
});

//subadmin
Breadcrumbs::register('sub_admin_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('Sub Admin', route(Auth::getDefaultDriver() . '.subadmin.index'));
});
Breadcrumbs::register('sub_admin_create', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('sub_admin_list');
	$breadcrumbs->push('SubAdmin Create', route(Auth::getDefaultDriver() . '.subadmin.create', $id));
});
Breadcrumbs::register('sub_admin_edit', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('sub_admin_list');
	$breadcrumbs->push('SubAdmin Edit', route(Auth::getDefaultDriver() . '.subadmin.edit', $id));
});



// Role Management -------------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('roles_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('Roles', route(Auth::getDefaultDriver() . '.roles.index'));
});
Breadcrumbs::register('roles_create', function ($breadcrumbs) {
	$breadcrumbs->parent('roles_list');
	$breadcrumbs->push('Add New Role', route(Auth::getDefaultDriver() . '.roles.create'));
});
Breadcrumbs::register('roles_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('roles_list');
	$breadcrumbs->push(__('Edit Role'), route('admin.roles.edit', $id));
});

// countries -------------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('countries_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('Countries', route(Auth::getDefaultDriver() . '.countries.index'));
});
Breadcrumbs::register('countries_create', function ($breadcrumbs) {
	$breadcrumbs->parent('countries_list');
	$breadcrumbs->push('Add New Country', route(Auth::getDefaultDriver() . '.countries.create'));
});

Breadcrumbs::register('countries_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('countries_list');
	$breadcrumbs->push('Edit Country', route(Auth::getDefaultDriver() . '.countries.edit', $id));
});

// states -------------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('states_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('States', route(Auth::getDefaultDriver() . '.states.index'));
});
Breadcrumbs::register('states_create', function ($breadcrumbs) {
	$breadcrumbs->parent('states_list');
	$breadcrumbs->push('Add New State', route(Auth::getDefaultDriver() . '.states.create'));
});

Breadcrumbs::register('states_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('states_list');
	$breadcrumbs->push('Edit State', route(Auth::getDefaultDriver() . '.states.edit', $id));
});

// cities -------------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('cities_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push('Cities', route(Auth::getDefaultDriver() . '.cities.index'));
});
Breadcrumbs::register('cities_create', function ($breadcrumbs) {
	$breadcrumbs->parent('cities_list');
	$breadcrumbs->push('Add New City', route(Auth::getDefaultDriver() . '.cities.create'));
});

Breadcrumbs::register('cities_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('cities_list');
	$breadcrumbs->push('Edit City', route(Auth::getDefaultDriver() . '.cities.edit', $id));
});

// CMS Pages ---------------------------------------------------------------------------------------------------------------------------------------------------
Breadcrumbs::register('cms_list', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('CMS Pages'), route('admin.pages.index'));
});
Breadcrumbs::register('cms_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('cms_list');
	$breadcrumbs->push(__('Edit CMS Page'), route('admin.pages.edit', $id));
});
//site configuartion
Breadcrumbs::register('site_setting', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Site Configuration'), route('admin.settings.index'));
});


//category breadcome 
Breadcrumbs::register('category', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Categories'), route('admin.category.index'));
});
Breadcrumbs::register('category_create', function ($breadcrumbs) {
	$breadcrumbs->parent('category');
	$breadcrumbs->push('Add New Category', route('admin.category.create'));
});

Breadcrumbs::register('category_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('category');
	$breadcrumbs->push('Edit Category', route('admin.category.edit', $id));
});


//ingredient module 
Breadcrumbs::register('ingredients', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Ingredient'), route('admin.ingredients.index'));
});
Breadcrumbs::register('ingredients_create', function ($breadcrumbs) {
	$breadcrumbs->parent('ingredients');
	$breadcrumbs->push('Add New Ingredients', route('admin.ingredients.create'));
});

Breadcrumbs::register('ingredients_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('ingredients');
	$breadcrumbs->push('Edit Ingredient', route('admin.ingredients.edit', $id));
});

//quantity module 
Breadcrumbs::register('quantity', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Quantity'), route('admin.quantity.index'));
});
Breadcrumbs::register('quantity_create', function ($breadcrumbs) {
	$breadcrumbs->parent('quantity');
	$breadcrumbs->push('Add New Quantity', route('admin.quantity.create'));
});
Breadcrumbs::register('quantity_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('quantity');
	$breadcrumbs->push('Edit Quantity', route('admin.quantity.edit', $id));
});

//measurment module
Breadcrumbs::register('measurement', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Measurment'), route('admin.measurement.index'));
});
Breadcrumbs::register('measurement_create', function ($breadcrumbs) {
	$breadcrumbs->parent('measurement');
	$breadcrumbs->push('Add New measurement', route('admin.measurement.create'));
});

Breadcrumbs::register('measurement_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('measurement');
	$breadcrumbs->push('Edit Measurment', route('admin.measurement.edit', $id));
});


//recipe module 

Breadcrumbs::register('recipe', function ($breadcrumbs) {
	$breadcrumbs->parent('dashboard');
	$breadcrumbs->push(__('Recipe'), route('admin.recipe.index'));
});
Breadcrumbs::register('recipe_create', function ($breadcrumbs) {
	$breadcrumbs->parent('recipe');
	$breadcrumbs->push('Add New Recipe', route('admin.recipe.create'));
});

Breadcrumbs::register('recipe_update', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('recipe');
	$breadcrumbs->push('Edit Recipe', route('admin.recipe.edit', $id));
});

Breadcrumbs::register('recipe_view', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('recipe');
	$breadcrumbs->push('View Recipe', route('admin.recipe.show', $id));
});
