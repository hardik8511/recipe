<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeDirectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_directions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("recipe_id")->nullable();


            $table->string("title")->nullable();
            $table->integer("order")->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->foreign("recipe_id")->references("id")->on("recipes")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_directions');
    }
}
