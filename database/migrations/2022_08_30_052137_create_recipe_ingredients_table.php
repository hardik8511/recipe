<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_ingredients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("recipe_id")->nullable();
            $table->foreign("recipe_id")->references("id")->on("recipes")->onDelete("cascade");

            $table->integer("order");

            $table->unsignedBigInteger("quantity_id")->nullable();
            $table->foreign("quantity_id")->references("id")->on("quantity")->onDelete("SET NULL");

            $table->unsignedBigInteger("measurment_id")->nullable();
            $table->foreign("measurment_id")->references("id")->on("measurment")->onDelete("SET NULL");

            $table->unsignedBigInteger("ingredients_id")->nullable();
            $table->foreign("ingredients_id")->references("id")->on("ingredients")->onDelete("SET NULL");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_ingredients');
    }
}
