<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislikes', function (Blueprint $table) {
            $table->id();
            $table->string("custom_id")->nullable();


            $table->unsignedBigInteger("recipe_id")->nullable();
            $table->foreign('recipe_id')->references('id')->on('recipes')->onUpdate('cascade')->onDelete('SET NULL');

            $table->unsignedBigInteger("admin_id")->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onUpdate('cascade')->onDelete('SET NULL');

            $table->enum("is_status", ["like", "dislike"])->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislikes');
    }
}
