<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->string('custom_id')->nullable();

            //foreign keys conversation 
            $table->unsignedBigInteger("admin_id")->nullable();
            $table->unsignedBigInteger("sub_category_id")->nullable();
            
            $table->string("title")->nullable();
            $table->string("preperation_time")->nullable();
            $table->string("cooking_time")->nullable();
            $table->string("total_time")->nullable();

            $table->enum('is_featured', ['y', 'n'])->default('n')->nullable();

            $table->integer("servings")->nullable();

            //ingredients 
            $table->string("fat")->nullable();
            $table->string("sodium")->nullable();
            $table->string("carbohydrates")->nullable();
            $table->string("sugar")->nullable();
            $table->string("protein")->nullable();
            $table->string("calories")->nullable();
            $table->string("fiber")->nullable();

            $table->enum('is_active', ['y', 'n'])->default('y')->nullable();
            $table->timestamps();
            $table->foreign("sub_category_id")->references("id")->on("sub_categories")->cascadeOnDelete();
            $table->foreign('admin_id')->references('id')->on('admins')->onUpdate('cascade')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}