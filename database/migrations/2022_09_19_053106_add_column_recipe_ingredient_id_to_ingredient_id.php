<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRecipeIngredientIdToIngredientId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ingredients', function (Blueprint $table) {
            //
            $table->unsignedBigInteger("recipe_ingredients_id")->nullable();
            $table->foreign("recipe_ingredients_id")->references("ingredients_id")->on("recipe_ingredients")->cascadeOnDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingredients', function (Blueprint $table) {
            //
            // $table->unsignedBigInteger("recipe_ingredients_id")->nullable();
        });
    }
}
