@extends('admin.auth.layouts.app')

@section('content')
    <div class="d-flex flex-column flex-root">
        <div class="login login-4 login-signin-on d-flex flex-row-fluid">
            <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
                style="background-image: url({{ asset('assets/media/bg/bg-3.jpg') }});">
                <div class="login-form text-center p-7 position-relative overflow-hidden" style="max-width: 450px;width:100%">
                    <!--begin::Login Header-->
                    <div class="d-flex flex-center mb-15">
                        <a href="#">
                            <img class="img-fluid" style="max-width: 240px; max-height: 60px;"
                                src="{{ asset($sitesetting['site_logo']) }}" alt="{{ env('APP_NAME') }}" />
                        </a>
                    </div>
                    <!--end::Login Header-->
                    <!--begin::Login Sign in form-->
                    <div class="login-signin">
                        <div class="mb-20">
                            <h3>Register</h3>
                            {{-- <div class="text-muted font-weight-bold"> Register: </div> --}}
                        </div>
                        @if (session('status'))
                            <p class="bg-success text-white py-2 px-4">{{ session('status') }}</p>
                        @endif
                        @if (session('danger'))
                            <p class="bg-danger text-white py-2 px-4">{{ session('danger') }}</p>
                        @endif
                        @if (session('success'))
                            <p class="bg-success text-white py-2 px-4">{{ session('success') }}</p>
                        @endif
                        <form id="frmLogin" method="POST" action="{{ route('sub_admin.registration.post') }}"
                            enctype="multipart/form-data" class="form">
                            @csrf
                            <div class="form-group mb-5 fv-plugins-icon-container">
                                <input type="text"
                                    class="form-control h-auto py-4 px-8 @error('full_name') is-invalid @enderror"
                                    id="full_name" name="full_name" value="{{ old('full_name') }}" placeholder="Name" />
                                @if ($errors->has('full_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group mb-5 fv-plugins-icon-container">
                                <input type="text"
                                    class="form-control h-auto py-4 px-8 @error('email') is-invalid @enderror"
                                    id="email" name="email" value="{{ old('email') }}" placeholder="Email" />
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group mb-5 fv-plugins-icon-container">
                                <input type="text"
                                    class="form-control h-auto py-4 px-8 @error('contact_no') is-invalid @enderror"
                                    id="contact_no" name="contact_no" value="{{ old('contact_no') }}"
                                    placeholder="Contact Number" />
                                @if ($errors->has('contact_no'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{-- <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="profile_photo" name="profile_photo" tabindex="0" />
                                    <label class="custom-file-label @error('profile_photo') is-invalid @enderror" for="customFile">Choose file</label>
                                    @if ($errors->has('profile_photo'))
                                        <span class="text-danger">
                                            <strong class="form-text">{{ $errors->first('profile_photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}
                            <div class="form-group mb-5 fv-plugins-icon-container">
                                <input type="password"
                                    class="form-control h-auto py-4 px-8 @error('password') is-invalid @enderror"
                                    id="password" name="password" placeholder="Password" />
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group mb-5 fv-plugins-icon-container">
                                <input type="password"
                                    class="form-control h-auto py-4 px-8 @error('password_confirmation') is-invalid @enderror"
                                    id="password_confirmation" name="password_confirmation"
                                    placeholder="Confirm Password" />
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{-- <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                                <div class="checkbox-inline">
                                    <label class="checkbox m-0 text-muted" for="remember_me">
                                        <input type="checkbox" id="remember_me" name="remember_me">
                                        <span></span>Remember me</label>
                                </div>
                                <a href="{{ route('sub_admin.forgot_password') }}"
                                    class="text-muted text-hover-primary">Forgot
                                    password ?</a>
                            </div> --}}
                            <button type="submit"
                                class="btn btn-pill btn-primary font-weight-bold px-9 py-4 my-3 mx-4 opacity-90 px-15 py-3">Register</button>
                        </form>

                    </div>
                    <div class="mt-10">
                        <p class="text-center text-muted">&copy; {{ \Carbon\Carbon::today()->format('F, Y') }} |
                            {{ config('app.name') }}. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('extra-js-scripts')
    <script src="{{ asset('admin/js/custom_validations.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#frmLogin").validate({
                rules: {
                    full_name: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                    },
                    email: {
                        required: true,
                        maxlength: 80,
                        email: true,
                        valid_email: true,
                        remote: {
                            url: "{{ route('check.email') }}",
                            type: "post",
                            data: {
                                _token: function() {
                                    return "{{ csrf_token() }}"
                                },
                                type: "admin",
                            }
                        },
                    },
                    contact_no: {
                        required: true,
                        not_empty: true,
                        maxlength: 16,
                        minlength: 6,
                        pattern: /^(\d+)(?: ?\d+)*$/,
                        remote: {
                            url: "{{ route('check.contact') }}",
                            type: "post",
                            data: {
                                _token: function() {
                                    return "{{ csrf_token() }}"
                                },
                                type: "admin",
                            }
                        },
                    },
                    password: {
                        required: true,
                        not_empty: true,
                        minlength: 8,
                    },
                    password_confirmation: {
                        required: true,
                        not_empty: true,
                        minlength: 8,
                        equalTo: "#password"
                    },
                    profile_photo: {
                        extension: "jpg|jpeg|png",
                    },
                },
                messages: {
                    full_name: {
                        required: "@lang('validation.required', ['attribute' => 'name'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'name'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'name', 'min' => 3])",
                    },
                    // last_name: {
                    //     required: "@lang('validation.required', ['attribute' => 'last name'])",
                    //     not_empty: "@lang('validation.not_empty', ['attribute' => 'last name'])",
                    //     minlength:"@lang('validation.min.string', ['attribute' => 'last name', 'min' => 3])",
                    // },
                    email: {
                        required: "@lang('validation.required', ['attribute' => 'email address'])",
                        maxlength: "@lang('validation.max.string', ['attribute' => 'email address', 'max' => 80])",
                        email: "@lang('validation.email', ['attribute' => 'email address'])",
                        valid_email: "@lang('validation.email', ['attribute' => 'email address'])",
                        remote: "@lang('validation.unique', ['attribute' => 'email address'])",
                    },
                    contact_no: {
                        required: "@lang('validation.required', ['attribute' => 'contact number'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'contact number'])",
                        maxlength: "@lang('validation.max.string', ['attribute' => 'contact number', 'max' => 16])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'contact number', 'min' => 6])",
                        pattern: "@lang('validation.numeric', ['attribute' => 'contact number'])",
                        remote: "@lang('validation.unique', ['attribute' => 'contact number'])",
                    },
                    password: {
                        required: "@lang('validation.required', ['attribute' => 'password '])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'password '])",
                        maxlength: "@lang('validation.max.string', ['attribute' => 'password ', 'max' => 16])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'password ', 'min' => 6])",

                    },
                    password_confirmation: {
                        required: "@lang('validation.required', ['attribute' => 'confirm password '])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'confirm password '])",
                        maxlength: "@lang('validation.max.string', ['attribute' => 'confirm password ', 'max' => 16])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'confirm password ', 'min' => 6])",
                        equalTo: "confirm password is must be same"
                    },
                    profile_photo: {
                        extension: "@lang('validation.mimetypes', ['attribute' => 'profile photo', 'value' => 'jpg|png|jpeg'])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $('#frmLogin').submit(function() {
                if ($(this).valid()) {
                    console.log("validated");
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endpush
