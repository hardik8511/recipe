@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('quantity_update', $quantity->id) !!}
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-list-ol text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">Edit {{ $custom_title }}</h3>
                </div>
            </div>

            <!--begin::Form-->
            <form id="frmQuantityForm" method="POST" action="{{ route('admin.quantity.update', $quantity->custom_id) }}"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">

                    {{-- First Name --}}
                    <div class="form-group">
                        <label for="no">{!! $mend_sign !!} Quantity: </label>
                        <input type="text" class="form-control @error('no') is-invalid @enderror" id="no"
                            name="no" value="{{ old('no') != null ? old('no') : $quantity->no }}"
                            placeholder="Enter Ingredient no" autocomplete="no" spellcheck="false"
                            autocapitalize="sentences" tabindex="0" autofocus />
                        @if ($errors->has('no'))
                            <span class="help-block">
                                <strong class="form-text">{{ $errors->first('no') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">Update {{ $custom_title }}</button>
                    <a href="{{ route('admin.quantity.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
@endsection

@push('extra-js-scripts')
    <script>
        $(document).ready(function() {
            jQuery.validator.addMethod(
                "special_number_only",
                function(value, element) {
                    return this.optional(element) || /^[0-9\/\.]+$/.test(value);
                },
                "enter a valida number"
            );

            $("#frmQuantityForm").validate({
                rules: {
                    no: {
                        required: true,
                        not_empty: true,
                        special_number_only: true,
                        remote: {
                            url: "{{ route('admin.check.name') }}",
                            type: "post",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: "quantity",
                                id: " {{ $quantity->id }} ",
                            }
                        },
                    },

                },
                messages: {
                    no: {
                        required: "@lang('validation.required', ['attribute' => 'quantity'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'quantity'])",
                        remote: "quantity is already taken",
                    },

                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            $('#frmQuantityForm').submit(function() {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });


        });
    </script>
@endpush
