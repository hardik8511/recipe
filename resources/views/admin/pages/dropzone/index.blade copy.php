<!DOCTYPE html>
<html>

<head>
    <title>PHP Dropzone File Upload on Button Click Example</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>PHP Dropzone File Upload on Button Click Example</h2>
                <form action="{{ route('admin.dropzoneFileUpload') }}" method="POST" enctype="multipart/form-data"
                    class="dropzone" id="image-upload">
                    @csrf
                    <div>
                        <h3>Upload Multiple Image By Click On Box</h3>
                    </div>
                </form>
                <button id="uploadFile">Upload Files</button>
            </div>
        </div>
    </div>



</body>
<script type="text/javascript">
    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone(".dropzone", {
        autoProcessQueue: false,
        maxFilesize: 5,
        maxFiles: 8,
        addRemoveLinks: true,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        maxfilesexceeded:"you can uplaod maximum 8 files",
        parallelUploads: 100,
        noDuplicates: false,
        dictDuplicateFile: "Duplicate Files Cannot Be Uploaded",
        preventDuplicates: true,

    });

    $('#uploadFile').click(function() {
    //   let dropzone_quaue =   myDropzone.processQueue();
    //   console.log(dropzone_quaue);
    //   return false;
    });
</script>

</html>
