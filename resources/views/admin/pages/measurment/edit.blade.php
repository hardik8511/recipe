@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('measurement_update', $measurement->id) !!}
@endpush
@push('extra-css-styles')
    <style>
        strong.form-text {
            color: red !important;
        }
    </style>
@endpush
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-thermometer-empty text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">Edit {{ $custom_title }}</h3>
                </div>
            </div>

            <!--begin::Form-->
            <form id="frmEditMeasurment" method="POST"
                action="{{ route('admin.measurement.update', $measurement->custom_id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">

                    {{-- First Name --}}
                    <div class="form-group">
                        <label for="name">{!! $mend_sign !!} Name:</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                            name="name" value="{{ old('name') != null ? old('name') : $measurement->name }}"
                            placeholder="Enter Measurment name" autocomplete="name" spellcheck="false"
                            autocapitalize="sentences" tabindex="0" autofocus />
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class="form-text">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">Update {{ $custom_title }}</button>
                    <a href="{{ route('admin.measurement.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
@endsection

@push('extra-js-scripts')
    <script>
        $(document).ready(function() {
            $("#frmEditMeasurment").validate({
                rules: {
                    name: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        only_alpha: true,
                        remote: {
                            url: "{{ route('admin.check.name') }}",
                            type: "post",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: "measurment",
                                id: " {{ $measurement->id }} ",
                            }
                        },
                    },

                },
                messages: {
                    name: {
                        required: "@lang('validation.required', ['attribute' => 'measurement'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'measurement'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'measurement', 'min' => 3])",
                        remote: "@lang('validation.unique', ['attribute' => 'measurment'])",
                        only_alpha: "Enter valid measurment name",
                    },

                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            $('#frmEditMeasurment').submit(function() {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });

        });
    </script>
@endpush
