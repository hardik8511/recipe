<div class="form-group ingredient_remove_div" >
    <div class="row ">
        <div class="col-md-3">
            <select name="recipe_quantity_data[${start_btn}]"
                class=" form-control custom_select_two_class classToValidate"
                id="">
                <option value=""> select quantity </option>
                @foreach ($recipe_quantity_data as $recipe_quantity)
                    <option value="{{ $recipe_quantity->id }}"> {{ $recipe_quantity->no }}
                    </option>
                @endforeach
            </select>
        </div>
    
        <div class="col-md-3">
            <select name="recipe_measurment_data[${start_btn}]"
                class="form-control custom_select_two_class classToValidate" id="">
                <option value=""> select measurment </option>
                @foreach ($recipe_measurment_data as $recipe_measurment)
                    <option value="{{ $recipe_measurment->id }}"> {{ $recipe_measurment->name }}
                    </option>
                @endforeach
            </select>
        </div>
    
        <div class="col-md-3">
            <select name="ingredients_name[${start_btn}]"
                class="form-control custom_select_two_class classToValidate" id="">
                <option value="">select ingredient</option>
                @foreach ($recipe_ingredient_data as $recipe_ingredient)
                    <option value="{{ $recipe_ingredient->id }}"> {{ $recipe_ingredient->name }}
                    </option>
                @endforeach
            </select>
        </div>
    
        <div class="col-md-3">
            <button class="btn btn-danger remove_btn_click" id="current_remove_html"> Remove
            </button>
        
        </div>
    </div>
</div>