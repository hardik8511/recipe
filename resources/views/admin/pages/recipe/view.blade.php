@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('recipe_view', $recipe->custom_id) !!}
@endpush

@push('extra-css-styles')
    <link rel="stylesheet" href="{{ asset('admin/css/owl.carousel.min.css') }}" />
    <style>
        .recipe_basic_data {
            width: 50%;
            float: left;
        }

        .recipe_img {
            width: 50%;
            float: left;
        }

        .recipe_nutrition {
            width: 50%;
            float: left;

        }

        .recipe_ingredients {
            width: 50%;
            float: left;
        }

        .owl-carousel .owl-item img {
            display: block;
            width: 97% !important;
        }
    </style>
@endpush

@section('content')

    <div class="container">
        <div class="card card-custom gutter-b draggable" tabindex="0">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Recipe Detail</h3>
                </div>
            </div>

            <div class="card-body">
                <h3> {{ $recipe->title ?? '' }} Details </h3>
                <div class="custom_div">
                    <div class="recipe_img">
                        @if ($recipe->images && $recipe->images->isNotEmpty())
                            <div class="owl-carousel owl-theme" style="border: 2px #edeff7">
                                @foreach ($recipe->images as $recipe_image)
                                    <div>
                                        <img height="250px" style="border-radius: 10px"
                                            src="{{ url('storage/recipe_img/' . $recipe_image->name) }}" alt="">
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>

                    <div class="recipe_basic_data">
                        <p> <i class="fa fa-clock"></i> <strong> Preperation Time : </strong>
                            {{ $recipe->preperation_time ?? '' }} </p>
                        <p> <i class="fa fa-clock"></i> <strong> Cooking Time : </strong> {{ $recipe->cooking_time ?? '' }}
                        </p>
                        <p> <i class="fa fa-clock"></i> <strong> Totat Time : </strong> {{ $recipe->total_time ?? '' }} </p>
                        <p> Is Featured : {{ $recipe->is_featured ?? '' }} </p>
                        <p> Servings : {{ $recipe->servings ?? '' }} </p>

                    </div>
                    <div>
                        Recipe Description : {{ $recipe->description ?? '-' }}
                    </div>

                </div>
            </div>
            <hr>
            <div class="card-body">
                <div class="recipe_ingredients">
                    <h4>Ingredients : </h4>

                    <table class="table table-hover table-bordered" style="width: 93%">
                        <thead>
                            <tr>
                                <th scope="col"> No </th>
                                <th scope="col"> Quantity </th>
                                <th scope="col">Measurment</th>
                                <th scope="col">Ingredient</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($recipe->ingredients->isNotEmpty())
                                @foreach ($recipe->ingredients as $key => $recipe_ingredient)
                                    <tr>
                                        <th scope="row"> {{ ++$key }} </th>
                                        <td> {{ $recipe_ingredient->quantity ? $recipe_ingredient->quantity->no : '-' }}
                                        </td>
                                        <td>{{ $recipe_ingredient->measurment ? $recipe_ingredient->measurment->name : '-' }}
                                        </td>
                                        <td> {{ $recipe_ingredient->ingredient ? $recipe_ingredient->ingredient->name : '-' }}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center"> No Ingredients Found </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="recipe_nutrition">
                    <h4>Recipe Nutritions: </h4>
                    <table style="width: 93%" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col"> No </th>
                                <th scope="col"> Name </th>
                                <th scope="col"> Value </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row"> 1 </th>
                                <td> Fat </td>
                                <td> {{ $recipe->fat ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 2 </th>
                                <td> Sodium </td>
                                <td> {{ $recipe->sodium ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 3 </th>
                                <td> Protein </td>
                                <td> {{ $recipe->protein ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 4 </th>
                                <td> Sugar </td>
                                <td> {{ $recipe->sugar ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 5 </th>
                                <td> Carbohydrates </td>
                                <td> {{ $recipe->carbohydrates ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 6 </th>
                                <td> Calories </td>
                                <td> {{ $recipe->calories ?? '-' }} </td>
                            </tr>
                            <tr>
                                <th scope="row"> 7 </th>
                                <td> Fiber </td>
                                <td> {{ $recipe->fiber ?? '-' }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card-body">

                <div class="recipe_direction">
                    <h4> Recipe Direction: </h4>
                    <hr>
                    @if ($recipe->directions->isNotEmpty())
                        @foreach ($recipe->directions as $key => $direction)
                            <p class="mb-1" style="font-weight: 600; font-size: 18px">
                                {{ ++$key }}. {{ $direction->title }}
                            </p>
                            <div style="margin-bottom: 10px">
                                {!! $direction->description !!}
                            </div>
                        @endforeach
                    @else
                        <label for="">No Direction Found</label>
                    @endif
                </div>

            </div>

        </div>
    </div>

@endsection

@push('extra-js-scripts')
    <script src="{{ asset('admin/js/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoPlaySpeed: 5000,
                autoPlayTimeout: 5000,
                autoplayHoverPause: false
            });
        });
    </script>
@endpush
