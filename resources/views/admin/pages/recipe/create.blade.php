@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('recipe_create') !!}
@endpush
@push('extra-css-styles')
    {{-- sortable js --}}
    <script src="{{ asset('admin/js/sortable.min.js') }}"></script>
    <style>
        .upload-button {
            height: 50px;
        }

        .jquery-uploader-select {
            position: relative;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .jquery-uploader-select-card {
            vertical-align: middle;
            width: 120px;
            height: 120px;
            border: 1px dashed rgba(103, 103, 103, 0.39);
            padding: 5px;
            margin: 5px;
            display: inline-block;
        }

        .jquery-uploader * {
            box-sizing: border-box;
        }

        .jquery-uploader-preview-container {
            /* padding: 10px; */
            height: 100%;
            width: 100%;
            background-color: white;
        }

        .upload_button_css {
            display: inline-block;
            float: right;
            margin: -16px -16px -22px 4px;
        }

        .upload_button_css input[type=file] {
            display: none;
        }

        .bootstrap-timepicker-hour,
        .bootstrap-timepicker-minute {
            margin-left: -35px !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">

            {{-- start first tag for recipe basic details --}}

            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-book text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">ADD {{ $custom_title }}</h3>
                </div>
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1"> Recipe </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">

                    {{-- start recipe basic information --}}
                    <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel">
                        <form id="frmAddRecipe" method="POST" action="{{ route('admin.recipe.store') }}"
                            enctype="multipart/form-data">
                            <input type="hidden" name="form_step" value="step_one">
                            @csrf
                            <div class="card-body">
                                {{-- First Name --}}
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="title">{!! $mend_sign !!} Title : </label>
                                        <input type="title" class="form-control @error('title') is-invalid @enderror"
                                            id="title" name="title" value="{{ old('title') }}"
                                            placeholder="Enter Recipe Title" autocomplete="title" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="title">{!! $mend_sign !!} Sub Category : </label>
                                        <select name="sub_category_id"
                                            class="form-control @error('sub_category_id') is-invalid @enderror"
                                            id="sub_category_id" data-error-container="#sub_category_error">
                                            <option value="" selected-default>Select Sub Category</option>
                                            @foreach ($sub_category_data as $key => $category)
                                                <optgroup label="{{ $category->name }}">
                                                    @foreach ($category->subcategories as $key => $sub_category)
                                                        <option value=" {{ $sub_category->id }} ">
                                                            {{ $sub_category->name }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <label id="sub_category_error">

                                        </label>

                                        @if ($errors->has('sub_category_id'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sub_category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    {{-- preparation time : preperation_time --}}
                                    <div class="form-group col">
                                        <label for="preperation_time">{!! $mend_sign !!} Preparation Time : </label>
                                        <div class="input-group timepicker">
                                            <input class="form-control total_time_calculation" name="preperation_time"
                                                value="{{ old('preperation_time') }}" id="preperation_time"
                                                readonly="readonly" placeholder="Select time" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @if ($errors->has('preperation_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('preperation_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{-- cooking time : cooking_time --}}
                                    <div class="form-group col">
                                        <label for="cooking_time">{!! $mend_sign !!} Cooking Time : <strong> in min
                                            </strong> </label>
                                        {{-- <input type="number"
                                            class="form-control total_time_calculation @error('cooking_time') is-invalid @enderror"
                                            id="cooking_time" name="cooking_time" value="{{ old('cooking_time') }}"
                                            placeholder="Enter Cooking Time" autocomplete="cooking_time" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus /> --}}
                                        <div class="input-group timepicker">
                                            <input class="form-control total_time_calculation" name="cooking_time"
                                                value="{{ old('cooking_time') }}" id="cooking_time" readonly="readonly"
                                                placeholder="Select time" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @if ($errors->has('cooking_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('cooking_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="total_time"> Total Time : <strong>In Min</strong> </label>
                                        <input type="text" class="form-control @error('total_time') is-invalid @enderror"
                                            id="total_time" name="total_time" value="{{ old('total_time') ?? '0' }}"
                                            placeholder="Total Time" autocomplete="total_time" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus readonly />
                                        @if ($errors->has('total_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('total_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                {{-- servings  : servings --}}
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="servings">{!! $mend_sign !!} Servings : </label>
                                        <input type="number" class="form-control @error('servings') is-invalid @enderror"
                                            id="servings" name="servings" value="{{ old('servings') }}"
                                            placeholder="Enter Servings " autocomplete="servings" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('servings'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('servings') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group col">
                                        <label for="description">{!! $mend_sign !!} Description : </label>
                                        <input type="text"
                                            class="form-control @error('description') is-invalid @enderror"
                                            id="description" name="description" value="{{ old('description') }}"
                                            placeholder="Enter description " autocomplete="description"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                </div>
                                {{-- <div class="form-row">
                                   
                                </div> --}}
                                <div class="card-title">
                                    <h4 class="card-label" style="text-align: left">Recipe Nutritions </h4>
                                    <hr>
                                </div>

                                <div class="form-row">
                                    {{-- sodium  : sodium --}}
                                    <div class="form-group col">
                                        <label for="fat"> Fat : <strong> in %</strong> </label>
                                        <input type="number" class="form-control @error('fat') is-invalid @enderror"
                                            id="fat" name="fat" value="{{ old('fat') }}"
                                            placeholder="Enter Fat" autocomplete="fat" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('fat'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('fat') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="sodium"> Sodium : <strong> gm </strong> </label>
                                        <input type="number" class="form-control @error('sodium') is-invalid @enderror"
                                            id="sodium" name="sodium" value="{{ old('sodium') }}"
                                            placeholder="Enter Sodium" autocomplete="sodium" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('sodium'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sodium') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{-- carbohydrates  : carbohydrates --}}
                                    <div class="form-group col">
                                        <label for="carbohydrates"> Carbohydrates : <strong>gm </strong>
                                        </label>
                                        <input type="number"
                                            class="form-control @error('carbohydrates') is-invalid @enderror"
                                            id="carbohydrates" name="carbohydrates" value="{{ old('carbohydrates') }}"
                                            placeholder="Enter Carbohydrates" autocomplete="carbohydrates"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('carbohydrates'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('carbohydrates') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    {{-- calories  : sugar --}}
                                    <div class="form-group col">
                                        <label for="sugar"> Sugar : <strong>gm</strong> </label>
                                        <input type="number" class="form-control @error('sugar') is-invalid @enderror"
                                            id="sugar" name="sugar" value="{{ old('sugar') }}"
                                            placeholder="Enter Sugar" autocomplete="sugar" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('sugar'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sugar') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{-- protein  : protein --}}
                                    <div class="form-group col">
                                        <label for="protein"> Protein : <strong>gm</strong> </label>
                                        <input type="number" class="form-control @error('protein') is-invalid @enderror"
                                            id="protein" name="protein" value="{{ old('protein') }}"
                                            placeholder="Enter Protein" autocomplete="protein" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('protein'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('protein') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">

                                    {{-- calories  : calories --}}
                                    <div class="form-group col">
                                        <label for="calories"> Calories : <strong>gm</strong> </label>
                                        <input type="number"
                                            class="form-control @error('calories') is-invalid @enderror" id="calories"
                                            name="calories" value="{{ old('calories') }}" placeholder="Enter Calories"
                                            autocomplete="calories" spellcheck="false" autocapitalize="sentences"
                                            tabindex="0" autofocus />
                                        @if ($errors->has('calories'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('calories') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{-- fiber  : fiber --}}
                                    <div class="form-group col ">
                                        <label for="fiber"> Fiber : <strong>gm</strong> </label>
                                        <input type="number" class="form-control @error('fiber') is-invalid @enderror"
                                            id="fiber" name="fiber" value="{{ old('fiber') }}"
                                            placeholder="Enter Fiber" autocomplete="fiber" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('fiber'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('fiber') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">

                                </div>

                                <div class="jquery-uploader-preview-container form-group">
                                    <label for="fiber">{!! $mend_sign !!} Upload Images : </label>
                                    <br>
                                    <span id="msg_for_more_img_add" style="color: red"></span>
                                    <div class="want_to_append_img_upload">
                                        <div class="jquery-uploader-select-card" data-custom_appended_id="0">
                                            <div class="jquery-uploader-select" style="cursor:copy"
                                                onclick="addMoreImages()">
                                                <div class="upload-button">
                                                    <strong>
                                                        <i class="fa fa-plus"></i> <br>
                                                        Add More
                                                    </strong>
                                                </div>
                                            </div>
                                            <div class="image-preview" id="img_preview_0" style="display: none">
                                                <img src="" width="106px" height="106px" id="file_preview_0"
                                                    alt="">
                                            </div>

                                        </div>
                                    </div>
                                    <span id="custom_validation_for_img" style="color: red;"> </span>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" id="btn_add_recipe" class="btn btn-primary mr-2 text-uppercase">
                                    Add
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@push('extra-js-scripts')
    {{-- time picker  --}}
    <script>
        var KTBootstrapTimepicker = function() {
            var demos = function() {
                $('#preperation_time,#cooking_time').timepicker({
                    minuteStep: 1,
                    defaultTime: '',
                    showMeridian: false,
                    snapToStep: true
                });
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTBootstrapTimepicker.init();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#sub_category_id").select2();

            $("#frmAddRecipe").validate({
                rules: {
                    title: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        only_alpha: true,
                    },
                    sub_category_id: {
                        required: true,
                    },
                    preperation_time: {
                        required: true,
                        not_empty: true,
                    },
                    description: {
                        required: true,
                        not_empty: true,
                        minlength: 10,
                        maxlength: 250,
                    },
                    servings: {
                        required: true,
                        not_empty: true,
                        digits: true,
                    },
                    "recipe_img[1]": {
                        required: true,
                        extension: "jpg|jpeg|png",
                    },
                },
                messages: {
                    title: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Title', 'min' => 3])",
                    },
                    sub_category_id: {
                        required: "Please Select The Sub Category",
                    },
                    preperation_time: {
                        required: "@lang('validation.required', ['attribute' => 'Preperation Time'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Preperation Time'])",
                    },
                    cooking_time: {
                        required: "@lang('validation.required', ['attribute' => 'cooking time'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'cooking time'])",
                    },
                    servings: {
                        required: "@lang('validation.required', ['attribute' => 'Servings'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Servings'])",
                    },
                    "recipe_img[1]": {
                        required: "@lang('validation.required', ['attribute' => 'Profile Photo'])",
                        extension: "@lang('validation.mimetypes', ['attribute' => 'Profile Photo', 'value' => 'jpg|png|jpeg'])",
                    },
                    description: {
                        required: "@lang('validation.required', ['attribute' => 'Description'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Description'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Description', 'min' => 10])",
                        maxlength: "@lang('validation.min.string', ['attribute' => 'Description', 'min' => 250])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    // $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger');
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $(document).on('keypress change keyup keydown', '.total_time_calculation', function(e) {
                let preparation_time = $("#preperation_time").val();
                let cooking_time = $("#cooking_time").val();
                let total_minutes = parseInt(preparation_time.slice(-2)) + parseInt(cooking_time.slice(-2));
                let total_hour = parseInt(preparation_time.substring(0, 2)) + parseInt(cooking_time
                    .substring(0, 2));

                if (total_minutes > 59) {
                    total_minutes = parseInt(total_minutes) - 60;
                    total_hour++;
                }

                if (!isNaN(total_hour) && !isNaN(total_minutes)) {
                    let new_val = total_hour + ":" + total_minutes;
                    $("#total_time").val(new_val);
                } else {
                    $("#total_time").val("0");
                }

            });

            $("#btn_add_recipe").click(function(e) {

                $('#frmAddRecipe').submit(function(e) {

                    let is_error = false;
                    $("[name^=recipe_img]").each(function() {
                        let custom_msg_id = $(this).data("custom_appended_id");
                        if ($(this).val()) {
                            $("#custom_img_msg_" + custom_msg_id).hide();
                        } else {
                            is_error = true;
                            $("#custom_img_msg_" + custom_msg_id).show();
                        }
                    });

                    // console.log(is_error);
                    let length_img = $(".remove_the_img").length;
                    if (length_img == 0) {
                        $("#custom_validation_for_img").text("upload at least one image");
                    }

                    if ($(this).valid() && is_error == false && length_img != 0) {
                        addOverlay();
                        $("input[type=submit], input[type=button], button[type=submit]").prop(
                            "disabled",
                            "disabled");
                        return true;
                    } else {
                        return false;
                    }
                });
            });

        });


        $(document).on('click', '.upload_img_recipe', function(e) {
            let dynamic_id = e.target.id;
            let append_id = $(this).data("custom_appended_id");
            // console.log(append_id);
            e.target.onchange = function(el) {
                if (el.target.files.length > 0) {
                    var uploaded_file_extension = el.target.files[0].type;
                    let valid_extension = ["image/png", "image/jpeg", "image/jpg"];

                    if (valid_extension.includes(uploaded_file_extension)) {
                        var src = URL.createObjectURL(el.target.files[0]);
                        var preview = document.getElementById("file_preview_" + append_id);
                        preview.src = src;
                        preview.style.display = "block";
                        $("#" + dynamic_id).parent().parent().next('.jquery-uploader-select').remove();
                        $("#img_preview_" + append_id).css("display", "block");
                        $("#custom_img_msg_" + append_id).hide();

                    } else {
                        $("#custom_img_msg_" + append_id).show();
                        $("#custom_img_msg_" + append_id).children().text("upload only image");
                        console.log("upload a valid extension file");
                    }

                }
            };
        });

        var images_start = 0;

        function addMoreImages() {

            var images_start = $(".jquery-uploader-select-card").length;
            if (images_start >= 9) {
                $("#msg_for_more_img_add").text("you can add maximum 8 images");
            } else {
                images_start = $('.jquery-uploader-select-card:last').data("custom_appended_id");
                images_start++;
                $(".want_to_append_img_upload").append(`@include('admin.pages.recipe.add_more_image')`);
                $("#custom_img_msg_" + images_start).hide();
                $("[name='recipe_img[" + images_start + "]']").rules("add", {
                    required: true,
                    extension: "jpg|jpeg|png",
                    messages: {
                        required: "please upload an image",
                        extension: "@lang('validation.mimetypes', ['attribute' => 'photo', 'value' => 'jpg|png|jpeg'])",
                    }
                });
            }
        }

        $(document).on('click', '.remove_the_img', function(e) {
            var images_start = $(".jquery-uploader-select-card").length;
            images_start--;
            $("#msg_for_more_img_add").text("");
            $(this).closest(".jquery-uploader-select-card").remove();
        });
    </script>
@endpush
