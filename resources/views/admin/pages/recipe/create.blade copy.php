@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('ingredients_create') !!}
@endpush
@push('extra-css-styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

    {{-- sortable js --}}
    <script src="{{ asset('admin/js/sortable.min.js') }}"></script>
    <style>
        .sortable-chosen {
            border: 2px dotted green;
            cursor: move;
        }

        .ingredient_remove_div:hover {
            cursor: move;
            border-color: blue;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">

            {{-- start first tag for recipe basic details --}}

            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-book text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">ADD {{ $custom_title }}</h3>
                </div>
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1">Recipe </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">

                    {{-- start recipe basic information --}}
                    <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel">
                        <form id="frmAddRecipe" method="POST" action="{{ route('admin.recipe.store') }}"
                            enctype="multipart/form-data">
                            <input type="hidden" name="form_step" value="step_one">
                            @csrf
                            <div class="card-body">
                                {{-- First Name --}}
                                <div class="form-group">
                                    <label for="title">{!! $mend_sign !!} Title : </label>
                                    <input type="title" class="form-control @error('title') is-invalid @enderror"
                                        id="title" name="title" value="{{ old('title') }}"
                                        placeholder="Enter Recipe Title" autocomplete="title" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- preparation time : preperation_time --}}
                                <div class="form-group">
                                    <label for="preperation_time">{!! $mend_sign !!} Preparation Time : <strong> in min
                                        </strong> </label>
                                    <input type="number"
                                        class="form-control @error('preperation_time') is-invalid @enderror"
                                        id="preperation_time" name="preperation_time" value="{{ old('preperation_time') }}"
                                        placeholder="Enter Preparation Time" autocomplete="preperation_time"
                                        spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('preperation_time'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('preperation_time') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- cooking time : cooking_time --}}
                                <div class="form-group">
                                    <label for="cooking_time">{!! $mend_sign !!} Cooking Time : <strong> in min
                                        </strong> </label>
                                    <input type="number" class="form-control @error('cooking_time') is-invalid @enderror"
                                        id="cooking_time" name="cooking_time" value="{{ old('cooking_time') }}"
                                        placeholder="Enter Cooking Time" autocomplete="cooking_time" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('cooking_time'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('cooking_time') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- servings  : servings --}}
                                <div class="form-group">
                                    <label for="servings">{!! $mend_sign !!} Servings : </label>
                                    <input type="number" class="form-control @error('servings') is-invalid @enderror"
                                        id="servings" name="servings" value="{{ old('servings') }}"
                                        placeholder="Enter Servings " autocomplete="servings" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('servings'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('servings') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- carbohydrates  : fat --}}
                                <div class="form-group">
                                    <label for="fat">{!! $mend_sign !!} Fat : <strong> in %</strong> </label>
                                    <input type="number" class="form-control @error('fat') is-invalid @enderror"
                                        id="fat" name="fat" value="{{ old('fat') }}" placeholder="Enter Fat"
                                        autocomplete="fat" spellcheck="false" autocapitalize="sentences" tabindex="0"
                                        autofocus />
                                    @if ($errors->has('fat'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('fat') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- sodium  : sodium --}}
                                <div class="form-group">
                                    <label for="sodium">{!! $mend_sign !!} Sodium : <strong> gm </strong> </label>
                                    <input type="number" class="form-control @error('sodium') is-invalid @enderror"
                                        id="sodium" name="sodium" value="{{ old('sodium') }}"
                                        placeholder="Enter Sodium" autocomplete="sodium" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('sodium'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('sodium') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- carbohydrates  : carbohydrates --}}
                                <div class="form-group">
                                    <label for="carbohydrates">{!! $mend_sign !!} Carbohydrates : <strong>gm </strong>
                                    </label>
                                    <input type="number"
                                        class="form-control @error('carbohydrates') is-invalid @enderror"
                                        id="carbohydrates" name="carbohydrates" value="{{ old('carbohydrates') }}"
                                        placeholder="Enter Carbohydrates" autocomplete="carbohydrates" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('carbohydrates'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('carbohydrates') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- calories  : sugar --}}
                                <div class="form-group">
                                    <label for="sugar">{!! $mend_sign !!} Sugar : <strong>gm</strong> </label>
                                    <input type="number" class="form-control @error('sugar') is-invalid @enderror"
                                        id="sugar" name="sugar" value="{{ old('sugar') }}"
                                        placeholder="Enter Sugar" autocomplete="sugar" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('sugar'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('sugar') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- protein  : protein --}}
                                <div class="form-group">
                                    <label for="protein">{!! $mend_sign !!} Protein : <strong>gm</strong> </label>
                                    <input type="number" class="form-control @error('protein') is-invalid @enderror"
                                        id="protein" name="protein" value="{{ old('protein') }}"
                                        placeholder="Enter Protein" autocomplete="protein" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('protein'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('protein') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- calories  : calories --}}
                                <div class="form-group">
                                    <label for="calories">{!! $mend_sign !!} Calories : <strong>gm</strong> </label>
                                    <input type="number" class="form-control @error('calories') is-invalid @enderror"
                                        id="calories" name="calories" value="{{ old('calories') }}"
                                        placeholder="Enter Calories" autocomplete="calories" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('calories'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('calories') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- fiber  : fiber --}}
                                <div class="form-group">
                                    <label for="fiber">{!! $mend_sign !!} Fiber : <strong>gm</strong> </label>
                                    <input type="number" class="form-control @error('fiber') is-invalid @enderror"
                                        id="fiber" name="fiber" value="{{ old('fiber') }}"
                                        placeholder="Enter Fiber" autocomplete="fiber" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('fiber'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('fiber') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>

                        </form>
                        <div class="container mt-2 dz-default dz-message">
                            <div class="row">
                                <div class="col-md-12 dz-clickable">
                                    <form action="{{ route('admin.dropzoneFileUpload') }}" method="post"
                                        enctype="multipart/form-data" id="image_upload_custom" class="dropzone mb-10"
                                        style="border-radius: 30px">
                                        @csrf
                                        <div style="" id="already_uploaded_img">

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="btn_add_recipe" class="btn btn-primary mr-2 text-uppercase"> Add
                                {{ $custom_title }}</button>
                            <a href="{{ route('admin.recipe.index') }}"
                                class="btn btn-secondary text-uppercase">Cancel</a>
                        </div>
                    </div>
                    {{-- end recipe basic information --}}

                </div>
            </div>
        </div>


    </div>
@endsection

@push('extra-js-scripts')
    {{-- start the dropzone script --}}
    <script type="text/javascript">
        Dropzone.autoDiscover = false;

        // var dropzone = new Dropzone(".dropzone", {
        var dropzone = new Dropzone(".dropzone", {
            maxFiles: 8,
            maxFilesize: 5,
            init: function() {
                this.on("uploadprogress", function(file, progress) {
                    console.log("File progress", progress);
                });
            },
            init: function() {
                this.on("maxfilesexceeded", function(file) {
                    console.log("maximum file excedeeds");
                });
            },
            preventDuplicates: true,
            dictDuplicateFile: "Duplicate Files Cannot Be Uploaded",
            renameFile: function(file) {
                // console.log();
                var dt = new Date();
                var time = dt.getTime();
                var random_digit = Math.floor(Math.random() * 100000) + 1;
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
                // console.log(extension);
                return time + random_digit + "." + extension;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function(file) {
                var name = file.upload.filename;
                let _token = "{{ csrf_token() }}";

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: name,
                        type: "local",
                        tmp_token: _token,

                    },
                    success: function(data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            success: function(file, response) {
                console.log(response);
            },
            error: function(file, response) {
                return false;
            }
        });

        //start  dropzone file remove 

        $(document).on('click', '.remove_uploaded_files', function(e) {
            var old_this = this;
            let fileName = $(this).data("id");
            if (fileName) {
                let _token = "{{ csrf_token() }}";
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: fileName,
                        type: "local",
                        tmp_token: _token,
                    },
                    success: function(data) {
                        $(old_this).closest(".dz-image-preview").remove();
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });

            }
        });

        //end dropzone file remove 
        //start fetch uploaded images 

        uploaded_images();

        function uploaded_images() {
            $.ajax({
                type: "get",
                url: "{{ route('admin.uploaded_img') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    
                },

                success: function(response) {
                    $("#already_uploaded_img").html(response);
                }
            });
        }

        //end fetch uploaded images 
    </script>
    {{-- end the dorpzone script --}}

    <script>
        $(document).ready(function() {
            $("#frmAddRecipe").validate({
                rules: {
                    title: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                    },
                },
                messages: {
                    title: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Title', 'min' => 3])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#btn_add_recipe").click(function(e) {
                $('#frmAddRecipe').submit();
            });

        });
    </script>
@endpush
