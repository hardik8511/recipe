@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('recipe_update', $recipe_data->id) !!}
@endpush
@push('extra-css-styles')
    <script src="{{ asset('admin/js/sortable.min.js') }}"></script>
    <style>
        .sortable-chosen {
            /* border: 2px dotted green; */
            cursor: move;
        }

        .ingredient_remove_div:hover {
            cursor: move;
            border-color: blue;
        }

        span.select2 {
            width: 100% !important;
        }

        /* start images upload css  */
        .upload-button {
            height: 50px;
        }

        .jquery-uploader-select {
            position: relative;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
        }

        .jquery-uploader-select-card {
            vertical-align: middle;
            width: 120px;
            height: 120px;
            border: 1px dashed rgba(103, 103, 103, 0.39);
            padding: 5px;
            margin: 5px;
            display: inline-block;
            /* border-radius: 10px */
        }

        .jquery-uploader * {
            box-sizing: border-box;
        }

        .jquery-uploader-preview-container {
            /* padding: 10px; */
            height: 100%;
            width: 100%;
            background-color: white;
            display: inline-block;
        }

        .upload_button_css {
            display: inline-block;
            float: right;
            margin: -19px -16px -22px 4px;
        }

        .upload_button_css input[type=file] {
            display: none;
        }

        .jquery-uploader-preview-container .want_to_append_img_upload {
            display: inline-block;
        }

        div.one_row {
            box-shadow: inset 0 11em 14em rgb(176 173 165 / 13%), 0 0 0 11px rgb(202 187 187 / 31%), -1.2em 0.3em 1em rgb(193 178 178 / 81%);
            border-radius: 12px;
            margin-top: 30px;
        }

        .one_row .form-group label {
            font-size: 1rem;
            font-weight: 400;
            color: #3F4254;
            margin: 5px 4px 7px 7px !important;
        }

        .one_row .form-group input {
            margin-left: 7px !important;
            width: 99% !important;
        }

        .one_row .form-group span {
            margin-left: 9px !important;
        }

        .note-editor.note-frame {
            border: 5px solid white;
            border-radius: 15px;
            padding: 0px 2px 2px 2px;
        }

        .custom_add_more_class {
            position: absolute;
            right: 10px;
            padding-bottom: 3px;
            margin-top: 3px;
            padding-top: 2px;
            padding-bottom: 2px;
            font-weight: 900;
            padding-right: 9px
        }

        /* #create_sortable_id { */
        /* pointer-events: none; */
        /* .sortable-chosen */
        /* } */

        /* end of images upload css  */
        .bootstrap-timepicker-hour,
        .bootstrap-timepicker-minute {
            margin-left: -35px !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">

            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-book text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">Edit {{ $custom_title }}</h3>
                </div>
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" id="step_first_form" href="#form_step_first">Recipe </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="step_two_form" href="#step_two">Ingredients</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="step_three_recipe_direction"
                                href="#step_three">Direction</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">

                    <div class="tab-pane fade" id="form_step_first" role="tabpanel">
                        <form id="frmEditRecipe_first_step" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_step" value="step_one">
                            {{ method_field('PUT') }}
                            <div class="card-body">

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="title">{!! $mend_sign !!} Title : </label>
                                        <input type="text" class="form-control @error('title') is-invalid @enderror"
                                            id="title" name="title"
                                            value="{{ old('title') != null ? old('title') : $recipe_data->title }}"
                                            placeholder="Enter Recipe Title" autocomplete="title" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">

                                        <label for="title">{!! $mend_sign !!} Sub Category : </label>
                                        <select name="sub_category_id"
                                            class="form-control @error('sub_category_id') is-invalid @enderror"
                                            id="sub_category_id" data-error-container="#sub_category_error">
                                            <option value="">select sub category</option>
                                            {{-- @foreach ($sub_category_data as $sub_category_item)
                                                <option value=" {{ $sub_category_item->id }} "
                                                    {{ $sub_category_item->id == $recipe_data->sub_category_id ? 'selected' : '' }}>
                                                    {{ $sub_category_item->name }}
                                                </option>
                                            @endforeach --}}

                                            @foreach ($sub_category_data as $key => $category)
                                                <optgroup label="{{ $category->name }}">
                                                    @foreach ($category->subcategories as $sub_category)
                                                        <option value=" {{ $sub_category->id }} "
                                                            {{ $sub_category->id == $recipe_data->sub_category_id ? 'selected' : '' }}>
                                                            {{ $sub_category->name }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <label id="sub_category_error">

                                        </label>

                                        @if ($errors->has('sub_category_id'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sub_category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">

                                    <div class="form-group col">
                                        <label for="preperation_time">{!! $mend_sign !!} Preparation Time :<strong> in
                                                min
                                            </strong> </label>
                                        {{-- <input type="text"
                                            class="form-control total_time_calculation @error('preperation_time') is-invalid @enderror"
                                            id="preperation_time" name="preperation_time"
                                            value="{{ old('preperation_time') != null ? old('preperation_time') : $recipe_data->preperation_time }}"
                                            placeholder="Enter Recipe Name" autocomplete="preperation_time"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus /> --}}
                                        <div class="input-group timepicker">
                                            <input class="form-control total_time_calculation" name="preperation_time"
                                                value="{{ old('preperation_time') != null ? old('preperation_time') : $recipe_data->preperation_time }}"
                                                id="preperation_time" readonly="readonly" placeholder="Select time"
                                                type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @if ($errors->has('preperation_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('preperation_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="form-group col">
                                        <label for="cooking_time">{!! $mend_sign !!} Cooking Time :<strong> in min
                                            </strong> </label>
                                        {{-- <input type="text"
                                            class="form-control total_time_calculation @error('cooking_time') is-invalid @enderror"
                                            id="cooking_time" name="cooking_time"
                                            value="{{ old('cooking_time') != null ? old('cooking_time') : $recipe_data->cooking_time }}"
                                            placeholder="Enter Cooking" autocomplete="cooking_time" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus /> --}}
                                        <div class="input-group timepicker">
                                            <input class="form-control total_time_calculation" name="cooking_time"
                                                value="{{ old('cooking_time') != null ? old('cooking_time') : $recipe_data->cooking_time }}"
                                                id="cooking_time" readonly="readonly" placeholder="Select time"
                                                type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @if ($errors->has('cooking_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('cooking_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="total_time"> Total Time : <strong>In Min</strong> </label>
                                        <input type="text" class="form-control @error('total_time') is-invalid @enderror"
                                            id="total_time" name="total_time"
                                            value="{{ $recipe_data->total_time ?? '0' }}" placeholder="Total Time"
                                            autocomplete="total_time" spellcheck="false" autocapitalize="sentences"
                                            tabindex="0" autofocus readonly />
                                        @if ($errors->has('total_time'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('total_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="servings">{!! $mend_sign !!} Servings : </label>
                                        <input type="number" class="form-control @error('servings') is-invalid @enderror"
                                            id="servings" name="servings"
                                            value="{{ old('servings') != null ? old('servings') : $recipe_data->servings }}"
                                            placeholder="Enter Servings" autocomplete="servings" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('servings'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('servings') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="description">{!! $mend_sign !!} Description : </label>
                                        <input type="text"
                                            class="form-control @error('description') is-invalid @enderror"
                                            id="description" name="description"
                                            value="{{ old('description') != null ? old('description') : $recipe_data->description }}"
                                            placeholder="Enter description " autocomplete="description"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>
                                {{-- <div class="form-row">
                                   
                                </div> --}}

                                <div class="card-title">
                                    <h4 class="card-label" style="text-align: left">Recipe Nutritions </h4>
                                    <hr>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="fat"> Fat : <strong> in %</strong> </label>
                                        <input type="number" class="form-control @error('fat') is-invalid @enderror"
                                            id="fat" name="fat"
                                            value="{{ old('fat') != null ? old('fat') : $recipe_data->fat }}"
                                            placeholder="Enter Fat" autocomplete="fat" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('fat'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('fat') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="sodium"> Sodium : <strong> gm </strong></label>
                                        <input type="text" class="form-control @error('sodium') is-invalid @enderror"
                                            id="sodium" name="sodium"
                                            value="{{ old('sodium') != null ? old('sodium') : $recipe_data->sodium }}"
                                            placeholder="Enter Sodium " autocomplete="sodium" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('sodium'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sodium') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group col">
                                        <label for="carbohydrates"> Carbohydrates :<strong> gm </strong>
                                        </label>
                                        <input type="text"
                                            class="form-control @error('carbohydrates') is-invalid @enderror"
                                            id="carbohydrates" name="carbohydrates"
                                            value="{{ old('carbohydrates') != null ? old('carbohydrates') : $recipe_data->carbohydrates }}"
                                            placeholder="Enter Carbohydrates" autocomplete="carbohydrates"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('carbohydrates'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('carbohydrates') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="sugar"> Sugar : <strong> gm </strong></label>
                                        <input type="text" class="form-control @error('sugar') is-invalid @enderror"
                                            id="sugar" name="sugar"
                                            value="{{ old('sugar') != null ? old('sugar') : $recipe_data->sugar }}"
                                            placeholder="Enter Sugar" autocomplete="sugar" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('sugar'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('sugar') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="form-group col">
                                        <label for="protein"> Protein : <strong> gm</strong> </label>
                                        <input type="text" class="form-control @error('protein') is-invalid @enderror"
                                            id="protein" name="protein"
                                            value="{{ old('protein') != null ? old('protein') : $recipe_data->protein }}"
                                            placeholder="Enter Protein" autocomplete="protein" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('protein'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('protein') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="calories"> Calories : <strong> gm </strong></label>
                                        <input type="text"
                                            class="form-control @error('calories') is-invalid @enderror" id="calories"
                                            name="calories"
                                            value="{{ old('calories') != null ? old('calories') : $recipe_data->calories }}"
                                            placeholder="Enter Calories" autocomplete="calories" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('calories'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('calories') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="form-group col">
                                        <label for="fiber"> Fiber : <strong> gm </strong></label>
                                        <input type="text" class="form-control @error('fiber') is-invalid @enderror"
                                            id="fiber" name="fiber"
                                            value="{{ old('fiber') != null ? old('fiber') : $recipe_data->fiber }}"
                                            placeholder="Enter Fiber" autocomplete="fiber" spellcheck="false"
                                            autocapitalize="sentences" tabindex="0" autofocus />
                                        @if ($errors->has('fiber'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('fiber') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="jquery-uploader-preview-container form-group">
                                    <label for="fiber">{!! $mend_sign !!} Upload Images : </label>
                                    <br>
                                    <span id="msg_for_more_img_add"
                                        style="color: red;display: block; margin-left: 9px;"></span>
                                    @php
                                        $recipe_image_last_key = 0;
                                        $total_keys = count($recipe_images);
                                    @endphp
                                    @if ($total_keys <= 7)
                                        <div class="want_to_append_img_upload">
                                            <div class="jquery-uploader-select-card" data-custom_appended_id="0"
                                                style="margin-left:10px">
                                                <div class="jquery-uploader-select" style="cursor:copy"
                                                    onclick="addMoreImages()">
                                                    <div class="upload-button">
                                                        <strong>
                                                            <i class="fa fa-plus"></i> <br>
                                                            Add More
                                                        </strong>
                                                    </div>
                                                </div>
                                                <div style="margin-top:9px; color:red ">
                                                    <span id="custom_msg_if_no_img"> </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($recipe_images->isNotEmpty())
                                        @foreach ($recipe_images as $key => $recipe_image_item)
                                            @php
                                                $recipe_image_last_key = $key + 1;
                                            @endphp
                                            <div class="want_to_append_img_upload">
                                                <div class="jquery-uploader-select-card"
                                                    data-custom_appended_id="{{ $key }}"
                                                    style="margin-left: 10px">
                                                    <div class="remove_the_img" style="margin: -15px -7px -6px -14px;">
                                                        <span style="cursor: pointer"> <i style="font-size: 20px"
                                                                class="fa fa-times"></i> </span>
                                                        @if ($recipe_image_item->name)
                                                            <span class="recipe_img_for_delete"
                                                                data-value="{{ $recipe_image_item->name }}"></span>
                                                        @endif

                                                    </div>

                                                    <div class="upload_button_css">
                                                        <label>
                                                            <input type="file" name="recipe_img[{{ $key }}]"
                                                                data-custom_appended_id="{{ $key }}"
                                                                accept="image/*" class="upload_img_recipe"
                                                                id="recipe_img_{{ $key }}">

                                                            <span style="cursor: pointer"> <i class="fa fa-pen"></i>
                                                            </span>
                                                        </label>
                                                    </div>

                                                    <div class="image-preview" id="img_preview_{{ $key }}">
                                                        @php
                                                            $is_exist = Storage::exists('recipe_img/' . $recipe_image_item->name);
                                                        @endphp
                                                        @if ($recipe_image_item->name && $is_exist)
                                                            <img src="{{ url('storage/recipe_img/' . $recipe_image_item->name) }} "
                                                                width="106px" height="106px"
                                                                id="file_preview_{{ $key }}" alt="">
                                                        @else
                                                            <img width="106px" height="106px"
                                                                id="file_preview_{{ $key }}" alt="">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif



                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btn_update_recipe_first"
                                    class="btn btn-primary mr-2 text-uppercase">
                                    Update
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="step_two" role="tabpanel">
                        <form id="frmEditIngredient" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PUT') }}
                            <input type="hidden" name="form_step" value="step_two">
                            <div style="margin-bottom: 10px">
                                <label for="">Recipe Ingredients : </label>
                                <button class="btn btn-primary" id="add_more_btn">
                                    Add Ingredients</button>
                            </div>

                            <div style="margin-bottom: 5px">
                                <span style="color: red" id="custom_msg_only_one_ingredient"></span>
                            </div>

                            <div class="form-group" id="create_sortable_id">
                                @if ($all_recipe_ingredient_data->isNotEmpty())
                                    <div class="row" style="pointer-events: none;">
                                        <label for="" class="col-md-3"> Quantitys : </label>
                                        <label for="" class="col-md-3"> Measurments : </label>
                                        <label for="" class="col-md-3"> Ingredients :</label>
                                        <label for="" class="col-md-3" style="visibility: hidden"> dfad </label>
                                    </div>
                                    @foreach ($all_recipe_ingredient_data as $key => $r_ingredient_item)
                                        @php
                                            ++$key;
                                        @endphp
                                        <div class="form-group ingredient_remove_div">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select name="recipe_quantity_data[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value="">select quantity </option>
                                                        @foreach ($recipe_quantity_data as $recipe_quantity)
                                                            <option value="{{ $recipe_quantity->id }}"
                                                                {{ $r_ingredient_item->quantity_id == $recipe_quantity->id ? 'selected' : '' }}>
                                                                {{ $recipe_quantity->no }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="recipe_measurment_data[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value=""> select measurment </option>
                                                        @foreach ($recipe_measurment_data as $recipe_measurment)
                                                            <option value="{{ $recipe_measurment->id }}"
                                                                {{ $r_ingredient_item->measurment_id == $recipe_measurment->id ? 'selected' : '' }}>
                                                                {{ $recipe_measurment->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="ingredients_name[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">

                                                        <option value="">select ingredient</option>
                                                        @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                                            <option value="{{ $recipe_ingredient->id }}"
                                                                {{ $r_ingredient_item->ingredients_id == $recipe_ingredient->id ? 'selected' : '' }}>
                                                                {{ $recipe_ingredient->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">

                                                    <button class="btn btn-danger remove_btn_click"
                                                        id='current_remove_html'>
                                                        Remove </button>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    @php
                                        $key == 0;
                                    @endphp
                                    <div class="row" style="pointer-events: none;">
                                        <label for="" class="col-md-3"> Quantitys : </label>
                                        <label for="" class="col-md-3"> Measurments : </label>
                                        <label for="" class="col-md-3"> Ingredients :</label>
                                        </label>
                                    </div>

                                    <div class="form-group ingredient_remove_div">
                                        <div class="row">
                                            <div class="col-md-3">
                                                {{-- <label for="">Quantity</label> --}}
                                                <select name="recipe_quantity_data['0']"
                                                    class="form-control custom_select_two_class required" id="">
                                                    <option value="">select quantity </option>
                                                    @foreach ($recipe_quantity_data as $recipe_quantity)
                                                        <option value="{{ $recipe_quantity->id }}">
                                                            {{ $recipe_quantity->no }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">

                                                <select name="recipe_measurment_data['0']"
                                                    class="form-control custom_select_two_class required" id="">
                                                    <option value=""> select measurment </option>
                                                    @foreach ($recipe_measurment_data as $recipe_measurment)
                                                        <option value="{{ $recipe_measurment->id }}">
                                                            {{ $recipe_measurment->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <select name="ingredients_name['0']"
                                                    class="form-control custom_select_two_class required" id="">

                                                    <option value="">select ingredient</option>
                                                    @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                                        <option value="{{ $recipe_ingredient->id }}">
                                                            {{ $recipe_ingredient->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <button class="btn btn-danger remove_btn_click" id='current_remove_html'>
                                                    Remove </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="">
                                <button type="submit" id="btn_update_recipe" class="btn btn-primary text-uppercase">
                                    Update
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>



                    <div class="tab-pane fade" id="step_three" role="tabpanel">
                        <div style="margin-bottom: 5px">
                            <button class="btn btn-primary btn_add_more_direction" id="add_more_direction">
                                Add Direction</button>
                        </div>
                        <span style="color: red; margin-left: 5px" id="custom_direction_msg"> </span>
                        <form id="frmEditRecipeDirection" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PUT') }}
                            <input type="hidden" name="form_step" value="step_three">
                            <div id="direction_all_append">
                                @if ($recipe_direction_data->isNotEmpty())
                                    @foreach ($recipe_direction_data as $key => $recipe_direction_item)
                                        <div class="one_row div_remove_direction" style="margin-top: 25px">
                                            @php
                                                $last_keys_for_direction = ++$key;
                                            @endphp
                                            <div class="form-group" style="position: relative">
                                                <button class="btn custom_add_more_class btn-danger btn_remove_direction ">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </button>
                                            </div>

                                            <div class="form-group">
                                                <label for="title">{!! $mend_sign !!} Title
                                                    </strong> </label>
                                                <input type="text"
                                                    class="form-control @error('title') is-invalid @enderror"
                                                    id="title_{{ $key }}" name="title[{{ $key }}]"
                                                    placeholder="Enter Recipe Name" autocomplete="title"
                                                    spellcheck="false" autocapitalize="sentences"
                                                    value="{{ old('title') != null ? old('title') : $recipe_direction_item->title }}"
                                                    tabindex="0" autofocus />
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong class="form-text">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="description">{!! $mend_sign !!} Description</label>
                                                <textarea class="form-control " id="description_{{ $key }}" name="description[{{ $key }}]"
                                                    placeholder="Enter description" autocomplete="description" spellcheck="true"> {{ $recipe_direction_item->description }} </textarea>
                                                @if ($errors->has('description'))
                                                    <span class="text-danger">
                                                        <strong
                                                            class="form-text">{{ $errors->first('description') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    @php
                                        $last_keys_for_direction = 0;
                                    @endphp
                                    <div class="one_row div_remove_direction">
                                        <div class="form-group" style="position: relative">
                                            <button class="btn btn-danger btn_remove_direction custom_add_more_class"><i
                                                    class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </div>

                                        <div class="form-group">
                                            <label for="title">{!! $mend_sign !!} Title
                                                </strong> </label>
                                            <input type="text"
                                                class="form-control @error('title') is-invalid @enderror" id="title_0"
                                                name="title[0]" placeholder="Enter Recipe Name" autocomplete="title"
                                                spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                    <strong class="form-text">{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="description">{!! $mend_sign !!} Description</label>
                                            <textarea class="form-control  @error('description') is-invalid @enderror" id="description_0" name="description[0]"
                                                placeholder="Enter description" autocomplete="description" spellcheck="true">
                                    </textarea>
                                            @if ($errors->has('description'))
                                                <span class="text-danger">
                                                    <strong class="form-text">{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                    </div>
                                @endif

                            </div>

                            <div class="">
                                <button type="submit" id="btn_update_recipe_direction"
                                    class="btn btn-primary text-uppercase">
                                    Update
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


    </div>
@endsection

@push('extra-js-scripts')
    <script src="{{ asset('admin/plugins/summernote/summernotecustom.js') }}"></script>
    <script src="{{ asset('admin/js/sortable.min.js') }}"></script>

    <script type="text/javascript">
        var summernoteImageUpload = '{{ route('admin.summernote.imageUpload') }}';
        var summernoteMediaDelete = '{{ route('admin.summernote.mediaDelete') }}';
    </script>

    <script>
        var KTBootstrapTimepicker = function() {
            var demos = function() {
                $('#preperation_time,#cooking_time').timepicker({
                    minuteStep: 1,
                    defaultTime: '',
                    showMeridian: false,
                    snapToStep: true
                });
            }
            return {
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTBootstrapTimepicker.init();
        });
    </script>

    <script type="text/javascript">
        //sortable div tag 
        var el1 = document.getElementById("create_sortable_id");
        var sortable = Sortable.create(el1);

        var el_direction = document.getElementById("direction_all_append");
        var sortable_direction = Sortable.create(el_direction);

        //start the add more direction 
        let direction_no = "{{ $last_keys_for_direction + 1 }}";
        $(document).on('click', '.btn_add_more_direction', function(e) {
            e.preventDefault();
            direction_no++;
            $("#custom_direction_msg").text("");
            $("#direction_all_append").append(`@include('admin.pages.recipe.add_more_direction')`);

            $("[name='title[" + direction_no + "]']").rules("add", {
                required: true,
                not_empty: true,
                minlength: 5,
                messages: {
                    required: "This Title Field Is Required.",
                    not_empty: "Only Space Is Not Allowed",
                    minlength: "Enter Minimum 5 Character",
                }
            });

            $("[name='description[" + direction_no + "]']").rules("add", {
                required: true,
                not_empty: true,
                minlength: 5,
                messages: {
                    required: "This Field Is Required.",
                    not_empty: "Only Space Is Not Allowed",
                    minlength: "Enter Minimum 5 Character",
                }
            });

            var summernoteElement = $("#description_" + direction_no);
            var imagePath = 'summernote/recipe/images';
            summernoteElement.summernote({
                height: 200,
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        for (var i = files.length - 1; i >= 0; i--) {
                            sendFile(files[i], this, imagePath);
                        }
                    },
                    onMediaDelete: function(target) {
                        deleteFile(target[0].src);
                    },
                }
            });
        });
        //end the add more direction 

        $(document).on('click', '.btn_remove_direction', function(e) {
            e.preventDefault();
            if ($(".div_remove_direction").length <= 1) {
                $("#custom_direction_msg").text("You can not remove one direction");
                console.log("Only One Direction You Can Not Remove");
            } else {
                $(this).closest(".div_remove_direction").remove();
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            jQuery.validator.setDefaults({
                ignore: [],
            });

            let which_step = "{{ session()->get('form_step_data') }}";
            if (which_step == "step_two_form") {
                $("#step_two_form").addClass("active");
                $("#step_two").addClass("active");
                $("#step_two").addClass("show");
                $("#form_step_first").removeClass("active show");
            } else if (which_step == "step_three_form") {
                $("#step_three_recipe_direction").addClass("active");
                $("#step_three").addClass("active");
                $("#step_three").addClass("show");
            } else {
                $("#step_first_form").addClass("active");
                $("#form_step_first").addClass("active");
                $("#form_step_first").addClass("show");
            }

            $(".custom_select_two_class").select2();
            $("#sub_category_id").select2();
            $("#frmEditRecipe").validate({
                rules: {
                    title: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                    },
                    description: {
                        required: true,
                        not_empty: true,
                        minlength: 10,
                        maxlength: 150,
                    },
                    preperation_time: {
                        required: true,
                        not_empty: true,
                        digits: true
                    },
                    cooking_time: {
                        required: true,
                        not_empty: true,
                        digits: true,
                    },
                    servings: {
                        required: true,
                        not_empty: true,
                        digits: true,
                    },
                    sub_category_id: {
                        required: true,
                    },
                },
                messages: {
                    title: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Title', 'min' => 3])",
                    },
                    description: {
                        required: "@lang('validation.required', ['attribute' => 'Description'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Description'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Description', 'min' => 10])",
                        maxlength: "The Description Maximum 150 Character",
                    },
                    sub_category_id: {
                        required: "Please Select The Sub Category",
                    },
                    preperation_time: {
                        required: "@lang('validation.required', ['attribute' => 'Preperation Time'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Preperation Time'])",
                    },
                    cooking_time: {
                        required: "@lang('validation.required', ['attribute' => 'Cooking Time'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Cooking Time'])",
                    },
                    servings: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger');
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#frmEditIngredient").validate({
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger');
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element.next());
                    }
                }
            });

            $("#frmEditRecipeDirection").validate({
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger');
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertBefore(element);
                    }
                }
            });

            //start add more ingredient function  
            $("#add_more_btn").click(function(e) {
                e.preventDefault();
                addMoreIngredient();
            });

            let new_btn = "{{ $key ?? '0' + 1 }}";

            function addMoreIngredient() {

                $("#custom_msg_only_one_ingredient").text("");
                $("#want_to_remove_label").hide();
                start_btn = parseInt(++new_btn);
                $("#create_sortable_id").append(`@include('admin.pages.recipe.add_more_recipe_ingredient')`);

                $(".custom_select_two_class").select2();
            }
            //end add more ingredient function

            $(document).on('click', '.remove_btn_click', function(e) {
                let count_total = (($("select[name^=recipe_quantity_data]").length));
                // console.log(count_total);
                if (count_total <= 1) {
                    $("#custom_msg_only_one_ingredient").text("You Can Not Remove All Ingredient");
                    return false;
                }
                e.preventDefault();
                $(this).closest(".ingredient_remove_div").remove();
            });


            $("#btn_update_recipe_first").click(function(e) {

                var is_error_img = false;
                $("[name^=recipe_img]").each(function() {
                    // console.log("hiiii");
                    let old_this = $(this);
                    let custom_msg_id = $(this).data("custom_appended_id");
                    if ($("#custom_img_msg_" + custom_msg_id).length) {
                        if (old_this.val()) {
                            $("#custom_img_msg_" + custom_msg_id).hide();
                        } else {
                            is_error_img = true;
                            $("#custom_img_msg_" + custom_msg_id).show();
                        }
                    }

                });

                let length_img = $(".remove_the_img").length;
                if (length_img == 0) {
                    $("#custom_msg_if_no_img").text("upload at least one image");
                }
                if (is_error_img == false && $(this).valid() && length_img != 0) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop(
                        "disabled",
                        "disabled");

                    $('#frmEditRecipe_first_step').submit();
                } else {
                    return false;
                }

            });


            $('#frmEditIngredient').submit(function() {
                $("[name^=recipe_quantity_data]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "Select Quantity Field Is Required.",
                        }
                    })
                });

                $("[name^=recipe_measurment_data]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "Select Measurment Field Is Required.",
                        }
                    })
                });
                $("[name^=ingredients_name]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "Select Ingredient Field Is Required.",
                        }
                    })
                });
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });





            //ckeditor 
            let ck_editors = "{{ $last_keys_for_direction }}";
            // console.log(ck_editors);
            for (let index = 0; index <= ck_editors; index++) {
                var summernoteElement = $('#description_' + index);

                $("[name='title[" + index + "]']").rules('add', {
                    required: true,
                    not_empty: true,
                    alpha_numeric: true,
                    messages: {
                        required: "Title Field Is Required.",
                        not_empty: "Only Space Is Not Allowed ",
                    }
                })

                $("[name='description[" + index + "]']").rules('add', {
                    required: true,
                    not_empty: true,
                    messages: {
                        required: "This Field Is Required.",
                        not_empty: "Only Space Is Not Allowed ",
                    }
                })


                var imagePath = 'summernote/recipe/images';
                summernoteElement.summernote({
                    not_empty: true,
                    height: 200,
                    callbacks: {
                        onImageUpload: function(files, editor, welEditable) {
                            for (var i = files.length - 1; i >= 0; i--) {
                                sendFile(files[i], this, imagePath);
                            }
                        },
                        onMediaDelete: function(target) {
                            deleteFile(target[0].src);
                        },
                    }
                });

            }
            //
            //tell the validator to ignore Summernote elements
            $('form').each(function() {
                if ($(this).data('validator'))
                    $(this).data('validator').settings.ignore = ".note-editor *";
            });
        });

        $(document).on('click', '#btn_update_recipe_direction', function() {
            $("[name^=title]").each(function() {
                console.log("hii hardik");
                $(this).rules('add', {
                    required: true,
                    not_empty: true,
                    alpha_numeric: true,
                    messages: {
                        required: "Title Field Is Required.",
                        not_empty: "Only Space Is Not Allowed ",
                    }
                })
            });
            $('#frmEditRecipeDirection').submit(function(e) {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop(
                        "disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });
        });

        //code for image upload
        $(document).on('click', '.remove_the_img', function(e) {
            var images_start = $(".jquery-uploader-select-card").length;
            images_start--;

            let file_name_to_delete = $(this).children('.recipe_img_for_delete').data("value");
            if (file_name_to_delete) {
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.recipeImageDelete') }}",
                    data: {
                        'recipe_img_name': file_name_to_delete,
                        '_token': "{{ csrf_token() }}",
                        'recipe_id': "{{ $recipe_data->id }}",
                    },
                    success: function(response) {
                        $("#msg_for_more_img_add").text("");
                        $(this).closest(".jquery-uploader-select-card").remove();
                    }
                });
            }
            $(this).closest(".jquery-uploader-select-card").remove();

        });

        $(document).on('click', '.upload_img_recipe', function(e) {
            let dynamic_id = e.target.id;
            // console.log(dynamic_id);
            let append_id = $(this).data("custom_appended_id");

            // console.log(append_id);
            e.target.onchange = function(el) {
                if (el.target.files.length > 0) {
                    // console.log(el.target.files[0].type);
                    var uploaded_file_extension = el.target.files[0].type;
                    let valid_extension = ["image/png", "image/jpeg", "image/jpg"];

                    if (valid_extension.includes(uploaded_file_extension)) {
                        var images_start = $(".jquery-uploader-select-card").length;
                        // if (images_start <= 8) {
                        //     addMoreImages();
                        // }

                        var src = URL.createObjectURL(el.target.files[0]);
                        var preview = document.getElementById("file_preview_" + append_id);
                        preview.src = src;
                        preview.style.display = "block";
                        $("#" + dynamic_id).parent().parent().next('.jquery-uploader-select').remove();
                        $("#img_preview_" + append_id).css("display", "block");
                    } else {
                        console.log("Upload A Valid Extension File");
                    }

                }
            };
        });

        function addMoreImages() {
            var images_start = $(".jquery-uploader-select-card").length;
            if (images_start >= 9) {
                $("#msg_for_more_img_add").text("You Can Add Maximum 8 Images");
            } else {
                images_start = $('.jquery-uploader-select-card:last').data("custom_appended_id");
                images_start++;
                $(".jquery-uploader-preview-container").append(`@include('admin.pages.recipe.add_more_image')`);
                $("#custom_img_msg_" + images_start).hide();

            }
        }

        $(document).on('keypress change keyup keydown', '.total_time_calculation', function(e) {
            // alert("hi");
            let preparation_time = $("#preperation_time").val();
            console.log(preparation_time);

            let cooking_time = $("#cooking_time").val();
            let total_minutes = parseInt(preparation_time.slice(-2)) + parseInt(cooking_time.slice(-2));
            let total_hour = parseInt(preparation_time.substring(0, 2)) + parseInt(cooking_time
                .substring(0, 2));

            if (total_minutes > 59) {
                total_minutes = parseInt(total_minutes) - 60;
                total_hour++;
            }
            // console.log(total_hour);
            if (!isNaN(total_hour) && !isNaN(total_minutes)) {
                let new_val = total_hour + ":" + total_minutes;
                $("#total_time").val(new_val);
            } else {
                $("#total_time").val("0");
            }

        });
    </script>
@endpush
