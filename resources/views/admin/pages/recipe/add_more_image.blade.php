<div class="jquery-uploader-select-card" id="custom_append_class" data-custom_appended_id="${images_start}"
    style="margin-left: 13px">
    <div class="remove_the_img" style="margin: -15px -7px -6px -15px;">
        <span style="cursor: pointer"> <i style="font-size: 20px" class="fa fa-times"></i> </span>
    </div>

    <div class="upload_button_css">
        <label>
            <input type="file" name="recipe_img[${images_start}]" accept="image/*" class="upload_img_recipe"
                data-custom_appended_id="${images_start}" id="recipe_img_${images_start}">
            <span style="cursor: pointer"> <i class="fa fa-pen"></i> </span>
        </label>
    </div>

    <label for="recipe_img_${images_start}" class="jquery-uploader-select">
        <div class="" style="cursor:copy">
            <div class="upload-button">
                <strong>
                    <i class="fa fa-upload"></i> <br>
                    Upload Image
                </strong>
            </div>
        </div>
    </label>

    <div class="image-preview" id="img_preview_${images_start}" style="display: none">
        <img src="" width="106px" height="106px" id="file_preview_${images_start}" alt="">
    </div>
    <div class="custom_validation_msg" style="margin-top: 3px;" id="custom_img_msg_${images_start}">
        <span style="color: red; margin-top: 10px"> Upload Image </span>
    </div>
</div>
