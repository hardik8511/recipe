@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('recipe') !!}
@endpush

@push('extra-css-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" />
    <style>
        .symbol.symbol-lg-75>img {
            width: 100%;
            max-width: 100% !important;
            height: 95px !important;
        }

        .custom_heart_class .heart {
            cursor: pointer;
            position: absolute;
            right: 15px;
            top: 8px;
            width: 36px;
            height: 36px;
            border-radius: 50%;
            border: 1px solid #f0f0f0;
            box-shadow: 0 1px 4px 0 rgb(0 0 0 / 10%);
            padding: 5px 7px 7px 6px;
            background: #fff;
        }

        ._1LmwT9 {
            display: inline-block;
            color: #c2c2c2;
            text-align: center;
            margin-left: 24px;
            font-size: 12px;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div style="margin-bottom: 10px; margin-left: 50px">
            <a href=" {{ route('admin.recipe.create') }} " class="btn btn-primary">Add New Recipe</a>
        </div>
        <div class="d-flex flex-column-fluid">

            <div class="container">
                <div class="d-flex flex-row">
                    <div class="flex-row-lg-fluid ml-lg-8">
                        <div class="want_to_append_recipe">
                            <div class="row">
                                @if ($recipies->isNotEmpty())
                                    @foreach ($recipies as $key => $recipe)
                                        <div class="col-xl-4 total_recipe_count">
                                            <div class="card card-custom gutter-b card-stretch">
                                                <div class="card-body pt-4 d-flex flex-column justify-content-between">
                                                    <div class="d-flex justify-content-start">
                                                        <div class="dropdown dropdown-inline" data-toggle="tooltip"
                                                            title="">
                                                            <a href="#"
                                                                class="btn btn-clean btn-hover-light-primary btn-sm btn-icon"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="ki ki-bold-more-hor"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                                                <ul class="navi navi-hover py-5">
                                                                    @if (auth()->user()->id == $recipe->admin_id || (auth()->user()->type = 'admin'))
                                                                        <li class="navi-item">

                                                                            <a href=" {{ route('admin.recipe.edit', ['recipe' => $recipe->custom_id]) }} "
                                                                                class="navi-link">
                                                                                <span class="navi-icon">
                                                                                    <i class="fa fa-edit"></i>
                                                                                </span>
                                                                                <span class="navi-text">Edit</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="navi-item">
                                                                            <a style="cursor: pointer"
                                                                                data-target-href=" {{ route('admin.recipe.destroy', ['recipe' => $recipe->custom_id]) }} " data-remove_recipe="remove"
                                                                                class="navi-link action-delete">
                                                                                <span class="navi-icon">
                                                                                    <i class="fa fa-trash"></i>
                                                                                </span>
                                                                                <span class="navi-text">Delete</span>
                                                                            </a>
                                                                        </li>
                                                                    @else
                                                                        <li class="navi-item">
                                                                            <a href="#" class="navi-link">
                                                                                <span class="navi-icon">
                                                                                    <i class="fa fa-edit"></i>
                                                                                </span>
                                                                                <span class="navi-text">Edit-No</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="navi-item">
                                                                            <a style="cursor: pointer" data-target-href="#"
                                                                                class="navi-link">
                                                                                <span class="navi-icon">
                                                                                    <i class="fa fa-trash"></i>
                                                                                </span>
                                                                                <span class="navi-text">Delete-No</span>
                                                                            </a>
                                                                        </li>
                                                                    @endif


                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-end custom_heart_class">
                                                        <div class="heart add_to_favorite_recipe"
                                                            data-recipe_id="{{ $recipe->id }}" data-toggle="tooltip"
                                                            title="" data-placement="left"
                                                            data-original-title="Favorite">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="23"
                                                                height="23" viewBox="0 0 20 16">
                                                                <path id="add_to_favorite_id{{ $recipe->id }}"
                                                                    d="M8.695 16.682C4.06 12.382 1 9.536 1 6.065 1 3.219 3.178 1 5.95 1c1.566 0 3.069.746 4.05 1.915C10.981 1.745 12.484 1 14.05 1 16.822 1 19 3.22 19 6.065c0 3.471-3.06 6.316-7.695 10.617L10 17.897l-1.305-1.215z"
                                                                    fill="{{ $recipe->favorites ? 'red' : '#c8c8c8' }} "
                                                                    class="eX72wL" stroke="#FFF" fill-rule="evenodd"
                                                                    opacity=".9"></path>
                                                            </svg>

                                                        </div>
                                                    </div>

                                                    <div class="d-flex flex-column flex-center">
                                                        @if ($recipe->image && $recipe->image->name)
                                                            <div class="bgi-no-repeat bgi-size-cover rounded min-h-180px w-100"
                                                                style="background-image: url({{ url('storage/recipe_img/' . $recipe->image->name) }})">
                                                            </div>
                                                        @else
                                                            <div class="bgi-no-repeat bgi-size-cover rounded min-h-180px w-100"
                                                                style="background-image: url({{ asset('assets/images/no-image.png') }})">
                                                            </div>
                                                        @endif

                                                        <a href="#"
                                                            class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">{{ $recipe->title ?? '' }}
                                                        </a>

                                                        <div class="font-weight-bold text-dark-50 font-size-sm pb-7">
                                                            {{ $recipe->subcategory ? $recipe->subcategory->name : '-' }}
                                                            @if ($recipe->is_featured == 'y')
                                                                <span style="font-weight:900"
                                                                    class="label label-inline label-md label-primary ">Assured</span>
                                                            @endif
                                                        </div>

                                                    </div>

                                                    <p class="mb-7"> {{ $recipe->description ?? '' }}
                                                    </p>

                                                    <p class="mb-7">
                                                        @if ($recipe->likedislike && $recipe->likedislike->is_status)
                                                            @if ($recipe->likedislike->is_status == 'like')
                                                                <img width="23px" class="icon_like_dislike"
                                                                    id="img_chage{{ $recipe->id }}"
                                                                    data-recipe_type="like"
                                                                    data-recipe_id="{{ $recipe->id }}" height="23px"
                                                                    src="{{ asset('assets/images/like_dark.png') }}"
                                                                    alt="">

                                                                <span
                                                                    class="badge badge-pill badge-success custom_count_like">
                                                                    {{ $recipe->likes ? count($recipe->likes) : '0' }}
                                                                </span>

                                                                <img class="icon_like_dislike" data-recipe_type="dislike"
                                                                    data-recipe_id="{{ $recipe->id }}" width="23px"
                                                                    id="img_chage{{ $recipe->id }}" height="23px"
                                                                    src="{{ asset('assets/images/dislike.png') }}"
                                                                    alt="">

                                                                <span
                                                                    class="badge badge-pill badge-warning custom_count_dislike">
                                                                    {{ $recipe->dislikes ? count($recipe->dislikes) : '0' }}
                                                                </span>
                                                            @elseif ($recipe->likedislike->is_status == 'dislike')
                                                                <img class="icon_like_dislike" data-recipe_type="like"
                                                                    id="img_chage{{ $recipe->id }}"
                                                                    data-recipe_id="{{ $recipe->id }}" width="23px"
                                                                    height="23px"
                                                                    src="{{ asset('assets/images/like_lite.png') }}"
                                                                    alt="">
                                                                <span
                                                                    class="badge badge-pill badge-success custom_count_like">
                                                                    {{ $recipe->likes ? count($recipe->likes) : '0' }}
                                                                </span>

                                                                <img width="23px" height="23px"
                                                                    class="icon_like_dislike"
                                                                    id="img_chage{{ $recipe->id }}"
                                                                    data-recipe_type="dislike"
                                                                    data-recipe_id="{{ $recipe->id }}"
                                                                    src="{{ asset('assets/images/dislike_dark.png') }}"
                                                                    alt="">
                                                                <span
                                                                    class="badge badge-pill badge-warning custom_count_dislike">
                                                                    {{ $recipe->dislikes ? count($recipe->dislikes) : '0' }}
                                                                </span>
                                                            @endif
                                                        @else
                                                            <img class="icon_like_dislike" data-recipe_type="like"
                                                                id="img_chage{{ $recipe->id }}"
                                                                data-recipe_id="{{ $recipe->id }}" width="23px"
                                                                height="23px"
                                                                src="{{ asset('assets/images/like_lite.png') }}"
                                                                alt="">
                                                            <span class="badge badge-pill badge-success custom_count_like">
                                                                {{ $recipe->likes ? count($recipe->likes) : '0' }} </span>

                                                            <img data-recipe_type="dislike" class="icon_like_dislike"
                                                                id="img_chage{{ $recipe->id }}"
                                                                data-recipe_id="{{ $recipe->id }}" width="23px"
                                                                height="23px"
                                                                src="{{ asset('assets/images/dislike.png') }}"
                                                                alt="">
                                                            <span
                                                                class="badge badge-pill badge-warning custom_count_dislike">
                                                                {{ $recipe->dislikes ? count($recipe->dislikes) : '0' }}
                                                            </span>
                                                        @endif
                                                    </p>

                                                    <div class="mb-7">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <span class="text-dark-75 font-weight-bolder mr-2">Preparatino
                                                                Time:</span>
                                                            <a href="#" class="text-muted text-hover-primary">
                                                                {{ $recipe->preperation_time ?? '0' }} </a>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-cente my-1">
                                                            <span class="text-dark-75 font-weight-bolder mr-2">Cooking
                                                                Time:</span>
                                                            <a href="#" class="text-muted text-hover-primary">
                                                                {{ $recipe->cooking_time ?? '0' }} </a>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <span class="text-dark-75 font-weight-bolder mr-2">Total
                                                                Time:</span>
                                                            <span class="text-muted font-weight-bold">
                                                                {{ $recipe->total_time ?? '0' }} </span>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <span
                                                                class="text-dark-75 font-weight-bolder mr-2">Servings:</span>
                                                            <span class="text-muted font-weight-bold">
                                                                {{ $recipe->servings ?? '0' }} </span>
                                                        </div>
                                                    </div>

                                                    <a href="{{ route('admin.recipe.show', $recipe->custom_id) }}"
                                                        class="btn btn-block btn-sm btn-light-warning font-weight-bolder text-uppercase py-4">View
                                                        Recipe</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <label for="">No Recipe Found </label>
                                @endif

                            </div>
                        </div>

                    </div>
                </div>

                <button class="btn btn-primary load_more_recipe" style="margin-bottom: 10px; margin-left: 22px"> Load More
                </button>
                {{-- <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-lg">
                        @if ($page == 1)
                            <li class="page-item disabled"> <a class="page-link" href="#"> Previous
                                </a> </li>
                        @else
                            <li class="page-item"> <a class="page-link"
                                    href="{{ route('admin.recipe.index', ['page' => $page - 1]) }}">Previous
                                </a> </li>
                        @endif
                        @for ($i = 1; $i <= $total_page; $i++)
                            <li class="page-item {{ $page == $i ? 'active disabled' : '' }} "><a class="page-link"
                                    href="{{ route('admin.recipe.index', ['page' => $i]) }} ">{{ $i }}
                                </a></li>
                        @endfor
                        @if ($total_page <= $page)
                            <li class="page-item disabled"> <a class="page-link" href="#"> Next
                                </a> </li>
                        @else
                            <li class="page-item"> <a class="page-link"
                                    href="{{ route('admin.recipe.index', ['page' => $page + 1]) }}">Next
                                </a> </li>
                        @endif
                    </ul>
                </nav> --}}
            </div>
        </div>
    </div>
@endsection

@push('extra-js-scripts')
    <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script>
        $(document).on('click', '.add_to_favorite_recipe', function() {
            let recipe_id = $(this).data("recipe_id");
            $.ajax({
                type: "post",
                url: "{{ route('admin.recipe.favorite') }}",
                data: {
                    recipe_id: recipe_id,
                    _token: "{{ csrf_token() }}",
                },
                success: function(response) {
                    if (response == "remove_from_favorite") {
                        toastr.success("remove from favorite");
                        $("#add_to_favorite_id" + recipe_id).attr('fill', '#c8c8c8');
                    } else if (response == "added_to_favorite") {
                        $("#add_to_favorite_id" + recipe_id).attr('fill', 'red');
                        toastr.success("added to favorite");
                    }
                }
            });

        });


        $(document).on('click', '.load_more_recipe', function() {

            let page_no = $(".page_load_more:last").val();
            page_no++;
            let is_last_page = $("#last_page_no_add_more").val();

            if (is_last_page == "yes") {
                $(this).hide();
            }

            $.ajax({
                type: "get",
                url: "{{ route('admin.recipe.index') }}",
                data: {
                    is_add_more: "yes",
                    limit: 3,
                    page: page_no ? page_no : "{{ $page + 2 }}",
                },
                success: function(response) {
                    $(".want_to_append_recipe").append(response);
                },

            });
        });


        //recipe like dislike 
        $(document).on('click', '.icon_like_dislike', function() {
            let recipe_id = $(this).data("recipe_id");
            let is_like = $(this).data("recipe_type");
            let old_this = $(this);
            $.ajax({
                type: "post",
                url: "{{ route('admin.recipe.likeDislike') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    recipe_id: recipe_id,
                    is_status: is_like,
                },
                success: function(response) {
                    console.log(response);
                    // return;
                    if (response.status == "recipe_dislike_sucessfully") {
                        toastr.success("dislike successfully");

                        old_this.attr("src", "{{ asset('assets/images/dislike_dark.png') }}");
                        old_this.prev().prev().attr("src",
                            "{{ asset('assets/images/like_lite.png') }}");
                        old_this.next().text(response.dislike_count);
                        old_this.prev().text(response.like_count);

                    }
                    if (response.status == "recipe_like_sucessfully") {
                        old_this.attr("src",
                            "{{ asset('assets/images/like_dark.png') }}");

                        old_this.next().next().attr("src", "{{ asset('assets/images/dislike.png') }}");
                        old_this.next().text(response.like_count);
                        old_this.next().next().next().text(response.dislike_count);

                        toastr.success("like successfully");
                    }
                    if (response.status == "already_liked") {
                        toastr.error("already liked");
                    }
                    if (response.status == "already_disliked") {
                        toastr.error("already disliked");
                    }

                }
            });

        });

        $(document).ready(function() {
            // datatable
            // console.log();

            oTable = $('#ingredient_id').DataTable({
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('admin.recipe.listing') }}",
                    data: {
                        columnsDef: ['checkbox', 'title', 'sub_category_id', 'preperation_time',
                            'cooking_time', 'active',
                            'action'
                        ],
                    },
                },
                columns: [{
                        data: 'checkbox'
                    },
                    {
                        data: 'title'
                    },
                    {
                        data: 'sub_category_id'
                    },
                    {
                        data: 'preperation_time'
                    },
                    {
                        data: 'cooking_time'
                    },
                    {
                        data: 'active'
                    },
                    {
                        data: 'action',
                        responsivePriority: -1
                    },
                ],
                columnDefs: [
                    // Specify columns titles here...
                    {
                        targets: 0,
                        title: "<center><input type='checkbox' class='all_select'></center>",
                        orderable: false
                    },
                    {
                        targets: 1,
                        title: ' Title',
                        orderable: true
                    },
                    {
                        targets: 2,
                        title: 'Sub Category',
                        orderable: true
                    },
                    {
                        targets: 3,
                        title: 'Preparation Time',
                        orderable: true
                    },
                    {
                        targets: 4,
                        title: 'Cooking Time',
                        orderable: true
                    },
                    {
                        targets: -2,
                        title: 'Active',
                        orderable: false
                    },
                    // Action buttons
                    {
                        targets: -1,
                        title: 'Action',
                        orderable: false
                    },
                ],
                order: [
                    [1, 'asc']
                ],
                lengthMenu: [
                    [10, 20, 50, 100],
                    [10, 20, 50, 100]
                ],
                pageLength: 10,
            });
        });
    </script>
@endpush
