<div style="" class="div_remove_direction one_row">
    <div class="form-group" style="position: relative">
        <button class="btn btn-danger btn_remove_direction custom_add_more_class">
            <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
    <div class="form-group">
        <label for="title">{!! $mend_sign !!} Title
            </strong> </label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title_${direction_no}"
            name="title[${direction_no}]" placeholder="Enter Recipe Name" autocomplete="title" spellcheck="false"
            autocapitalize="sentences" tabindex="0" autofocus />
        @if ($errors->has('title'))
            <span class="help-block">
                <strong class="form-text">{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
    {{-- Description --}}
    <div class="form-group">
        <label for="description">Description{!! $mend_sign !!}</label>
        <textarea class="form-control  @error('description') is-invalid @enderror"
            id="description_${direction_no}" name="description[${direction_no}]" placeholder="Enter description"
            class="ck_editor_dynamicalley" autocomplete="description" spellcheck="true">
    </textarea>
        @if ($errors->has('description'))
            <span class="text-danger">
                <strong class="form-text">{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
    {{-- <br style="border: 1px solid black"> --}}
</div>
