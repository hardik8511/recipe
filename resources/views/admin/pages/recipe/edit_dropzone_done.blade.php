@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('ingredients_create') !!}
@endpush
@push('extra-css-styles')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

    {{-- sortable js --}}
    <script src="{{ asset('admin/js/sortable.min.js') }}"></script>
    <style>
        .sortable-chosen {
            border: 2px dotted green;
            cursor: move;
        }

        .ingredient_remove_div:hover {
            cursor: move;
            border-color: blue;
        }

        span.select2 {
            width: 100% !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">

            {{-- start first tag for recipe basic details --}}
            <div class="card-header card-header-tabs-line">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-book text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">Edit {{ $custom_title }}</h3>
                </div>
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" id="step_first_form" href="#form_step_first">Recipe </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="step_two_form" href="#step_two">Ingredients</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" id="step_three_recipe_direction"
                                href="#step_three">Direction</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    {{-- start recipe basic information --}}
                    <div class="tab-pane fade" id="form_step_first" role="tabpanel">
                        <form id="frmEditRecipe" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_step" value="step_one">
                            {{ method_field('PUT') }}
                            <div class="card-body">
                                {{-- First Name --}}
                                <div class="form-group">
                                    <label for="title">{!! $mend_sign !!} Title : </label>
                                    <input type="text" class="form-control @error('title') is-invalid @enderror"
                                        id="title" name="title"
                                        value="{{ old('title') != null ? old('title') : $recipe_data->title }}"
                                        placeholder="Enter Recipe Title" autocomplete="title" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- preparation time : preperation_time --}}
                                <div class="form-group">
                                    <label for="preperation_time">{!! $mend_sign !!} Preparation Time :<strong> in min
                                        </strong> </label>
                                    <input type="text"
                                        class="form-control @error('preperation_time') is-invalid @enderror"
                                        id="preperation_time" name="preperation_time"
                                        value="{{ old('preperation_time') != null ? old('preperation_time') : $recipe_data->preperation_time }}"
                                        placeholder="Enter Recipe Name" autocomplete="preperation_time" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('preperation_time'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('preperation_time') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- cooking time : cooking_time --}}
                                <div class="form-group">
                                    <label for="cooking_time">{!! $mend_sign !!} Cooking Time :<strong> in min
                                        </strong> </label>
                                    <input type="text" class="form-control @error('cooking_time') is-invalid @enderror"
                                        id="cooking_time" name="cooking_time"
                                        value="{{ old('cooking_time') != null ? old('cooking_time') : $recipe_data->cooking_time }}"
                                        placeholder="Enter Cooking" autocomplete="cooking_time" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('cooking_time'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('cooking_time') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- servings  : servings --}}
                                <div class="form-group">
                                    <label for="servings">{!! $mend_sign !!} Servings : </label>
                                    <input type="number" class="form-control @error('servings') is-invalid @enderror"
                                        id="servings" name="servings"
                                        value="{{ old('servings') != null ? old('servings') : $recipe_data->servings }}"
                                        placeholder="Enter Servings" autocomplete="servings" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('servings'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('servings') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- carbohydrates  : fat --}}
                                <div class="form-group">
                                    <label for="fat">{!! $mend_sign !!} Fat : <strong> in %</strong> </label>
                                    <input type="text" class="form-control @error('fat') is-invalid @enderror"
                                        id="fat" name="fat"
                                        value="{{ old('fat') != null ? old('fat') : $recipe_data->fat }}"
                                        placeholder="Enter Fat" autocomplete="fat" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('fat'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('fat') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- sodium  : sodium --}}
                                <div class="form-group">
                                    <label for="sodium">{!! $mend_sign !!} Sodium : <strong> gm </strong></label>
                                    <input type="text" class="form-control @error('sodium') is-invalid @enderror"
                                        id="sodium" name="sodium"
                                        value="{{ old('sodium') != null ? old('sodium') : $recipe_data->sodium }}"
                                        placeholder="Enter Sodium " autocomplete="sodium" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('sodium'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('sodium') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- carbohydrates  : carbohydrates --}}
                                <div class="form-group">
                                    <label for="carbohydrates">{!! $mend_sign !!} Carbohydrates :<strong> gm </strong>
                                    </label>
                                    <input type="text"
                                        class="form-control @error('carbohydrates') is-invalid @enderror"
                                        id="carbohydrates" name="carbohydrates"
                                        value="{{ old('carbohydrates') != null ? old('carbohydrates') : $recipe_data->carbohydrates }}"
                                        placeholder="Enter Carbohydrates" autocomplete="carbohydrates" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('carbohydrates'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('carbohydrates') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- calories  : sugar --}}
                                <div class="form-group">
                                    <label for="sugar">{!! $mend_sign !!} Sugar : <strong> gm </strong></label>
                                    <input type="text" class="form-control @error('sugar') is-invalid @enderror"
                                        id="sugar" name="sugar"
                                        value="{{ old('sugar') != null ? old('sugar') : $recipe_data->sugar }}"
                                        placeholder="Enter Sugar" autocomplete="sugar" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('sugar'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('sugar') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- protein  : protein --}}
                                <div class="form-group">
                                    <label for="protein">{!! $mend_sign !!} Protein : </label>
                                    <input type="text" class="form-control @error('protein') is-invalid @enderror"
                                        id="protein" name="protein"
                                        value="{{ old('protein') != null ? old('protein') : $recipe_data->protein }}"
                                        placeholder="Enter Protein" autocomplete="protein" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('protein'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('protein') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- calories  : calories --}}
                                <div class="form-group">
                                    <label for="calories">{!! $mend_sign !!} Calories : <strong> gm </strong></label>
                                    <input type="text" class="form-control @error('calories') is-invalid @enderror"
                                        id="calories" name="calories"
                                        value="{{ old('calories') != null ? old('calories') : $recipe_data->calories }}"
                                        placeholder="Enter Calories" autocomplete="calories" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('calories'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('calories') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{-- fiber  : fiber --}}
                                <div class="form-group">
                                    <label for="fiber">{!! $mend_sign !!} Fiber : <strong> gm </strong></label>
                                    <input type="text" class="form-control @error('fiber') is-invalid @enderror"
                                        id="fiber" name="fiber"
                                        value="{{ old('fiber') != null ? old('fiber') : $recipe_data->fiber }}"
                                        placeholder="Enter Fiber" autocomplete="fiber" spellcheck="false"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                    @if ($errors->has('fiber'))
                                        <span class="help-block">
                                            <strong class="form-text">{{ $errors->first('fiber') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>

                        </form>
                        <div class="container dz-default dz-message">
                            <div class="row">
                                <div class="col-md-12 dz-clickable">
                                    <label for="image_upload_custom">{!! $mend_sign !!} Recipe Image : </label>
                                    <form action="{{ route('admin.dropzoneFileUpload') }}" method="post"
                                        enctype="multipart/form-data" id="image_upload_custom" class="dropzone mb-10"
                                        style="border-radius: 30px">
                                        <input type="hidden" name="recipe_id" value="{{ $recipe_data->id }}">
                                        @csrf
                                        <div style="" id="already_uploaded_img">

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="btn_update_recipe" class="btn btn-primary mr-2 text-uppercase">
                                Update
                                {{ $custom_title }}</button>
                            <a href="{{ route('admin.recipe.index') }}"
                                class="btn btn-secondary text-uppercase">Cancel</a>
                        </div>
                    </div>
                    {{-- end recipe basic information --}}

                    {{-- start the recipe ingredient --}}
                    <div class="tab-pane fade" id="step_two" role="tabpanel">
                        <form id="frmEditIngredient" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PUT') }}
                            <input type="hidden" name="form_step" value="step_two">
                            <div class="append_html_ingredient">
                                <div class="form-group">
                                    <label for="">Recipe Ingredient : </label>
                                    @forelse ($all_recipe_ingredient_data as  $key => $r_ingredient_item)
                                        <div class="form-group {{ $key == 0 ? '' : 'ingredient_remove_div' }}">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select name="recipe_quantity_data[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value="">select quantity </option>
                                                        @foreach ($recipe_quantity_data as $recipe_quantity)
                                                            <option value="{{ $recipe_quantity->id }}"
                                                                {{ $r_ingredient_item->quantity_id == $recipe_quantity->id ? 'selected' : '' }}>
                                                                {{ $recipe_quantity->no }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="recipe_measurment_data[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value=""> select measurment </option>
                                                        @foreach ($recipe_measurment_data as $recipe_measurment)
                                                            <option value="{{ $recipe_measurment->id }}"
                                                                {{ $r_ingredient_item->measurment_id == $recipe_measurment->id ? 'selected' : '' }}>
                                                                {{ $recipe_measurment->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="ingredients_name[{{ $key }}]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">

                                                        <option value="">select ingredient</option>
                                                        @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                                            <option value="{{ $recipe_ingredient->id }}"
                                                                {{ $r_ingredient_item->ingredients_id == $recipe_ingredient->id ? 'selected' : '' }}>
                                                                {{ $recipe_ingredient->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <button
                                                        class="btn btn-{{ $key == 0 ? 'primary' : 'danger remove_btn_click' }} "
                                                        id={{ $key == 0 ? 'add_more_btn' : 'current_remove_html' }}>
                                                        {{ $key == 0 ? 'Add More' : 'Remove' }} </button>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- @php
                                            $start_btn = $key;
                                        @endphp --}}
                                    @empty
                                        @php
                                            $key = 1;
                                        @endphp
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select name="recipe_quantity_data[0]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value="">select quantity </option>
                                                        @foreach ($recipe_quantity_data as $recipe_quantity)
                                                            <option value="{{ $recipe_quantity->id }}">
                                                                {{ $recipe_quantity->no }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="recipe_measurment_data[0]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value=""> select measurment </option>
                                                        @foreach ($recipe_measurment_data as $recipe_measurment)
                                                            <option value="{{ $recipe_measurment->id }}">
                                                                {{ $recipe_measurment->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <select name="ingredients_name[0]"
                                                        class="form-control custom_select_two_class required"
                                                        id="">
                                                        <option value="">select ingredient</option>
                                                        @foreach ($recipe_ingredient_data as $recipe_ingredient)
                                                            <option value="{{ $recipe_ingredient->id }}">
                                                                {{ $recipe_ingredient->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <button class="btn btn-primary" id="add_more_btn"> Add More </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>

                            <div class="append_tag" id="appended_tags">
                            </div>
                            <div class="">
                                <button type="submit" id="btn_update_recipe" class="btn btn-primary text-uppercase">
                                    Update
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>
                    {{-- end the recipe ingredietnt --}}

                    {{-- start the recipe direction --}}
                    <div class="tab-pane fade" id="step_three" role="tabpanel">
                        <form id="frmEditRecipeDirection" method="POST"
                            action="{{ route('admin.recipe.update', $recipe_data->custom_id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PUT') }}
                            <input type="hidden" name="form_step" value="step_three">
                            @forelse ($recipe_direction_data as  $key => $recipe_direction_item)
                                <div class="one_row div_remove_direction">
                                    @php
                                        $last_keys_for_direction = $key;
                                    @endphp
                                    <div class="form-group" style="position: relative">
                                        <button
                                            class="btn btn-{{ $key == 0 ? 'primary' : 'danger' }}  {{ $key == 0 ? 'btn_add_more_direction' : 'btn_remove_direction' }} "
                                            style="position: absolute; right: 10px; top: -17px;">
                                            {{ $key == 0 ? 'Add More' : 'Remove' }} </button>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">{!! $mend_sign !!} Title
                                            </strong> </label>
                                        <input type="text" class="form-control @error('title') is-invalid @enderror"
                                            id="title" name="title[{{ $key }}]"
                                            placeholder="Enter Recipe Name" autocomplete="title" spellcheck="false"
                                            autocapitalize="sentences"
                                            value="{{ old('title') != null ? old('title') : $recipe_direction_item->title }}"
                                            tabindex="0" autofocus />
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description{!! $mend_sign !!}</label>
                                        <textarea class="form-control description_error_get @error('description') is-invalid @enderror"
                                            id="description_{{ $key }}" name="description[{{ $key }}]" placeholder="Enter description"
                                            autocomplete="description" spellcheck="true"> {{ $recipe_direction_item->description }}
                                    </textarea>
                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                                <strong class="form-text">{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    {{-- </div> --}}
                                </div>
                            @empty
                                @php
                                    $last_keys_for_direction = 0;
                                @endphp
                                <div class="one_row">
                                    <div class="form-group" style="position: relative">
                                        <button class="btn btn-primary btn_add_more_direction"
                                            style="position: absolute; right: 10px; top: -17px;"> Add More </button>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">{!! $mend_sign !!} Title
                                            </strong> </label>
                                        <input type="text" class="form-control @error('title') is-invalid @enderror"
                                            id="title" name="title[0]" placeholder="Enter Recipe Name"
                                            autocomplete="title" spellcheck="false" autocapitalize="sentences"
                                            tabindex="0" autofocus />
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong class="form-text">{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description{!! $mend_sign !!}</label>
                                        <textarea class="form-control description_error_get @error('description') is-invalid @enderror" id="description_0"
                                            name="description[0]" placeholder="Enter description" autocomplete="description" spellcheck="true">
                                            </textarea>
                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                                <strong class="form-text">{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    {{-- </div> --}}
                                </div>
                            @endforelse
                            <div class="appended_direction_recipe">
                            </div>


                            <div class="">
                                <button type="submit" id="btn_update_recipe_direction"
                                    class="btn btn-primary text-uppercase">
                                    Update
                                    {{ $custom_title }}</button>
                                <a href="{{ route('admin.recipe.index') }}"
                                    class="btn btn-secondary text-uppercase">Cancel</a>
                            </div>
                        </form>
                    </div>
                    {{-- end the recipe direction --}}
                </div>
            </div>
        </div>


    </div>
@endsection

@push('extra-js-scripts')
    {{-- start the dropzone script --}}

    <script src="{{ asset('admin/plugins/summernote/summernotecustom.js') }}"></script>
    <script type="text/javascript">
        var summernoteImageUpload = '{{ route('admin.summernote.imageUpload') }}';
        var summernoteMediaDelete = '{{ route('admin.summernote.mediaDelete') }}';
    </script>

    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var dropzone = new Dropzone(".dropzone", {
            maxFiles: 8,
            maxFilesize: 5,
            preventDuplicates: true,
            renameFile: function(file) {
                // console.log();
                var dt = new Date();
                var time = dt.getTime();
                var random_digit = Math.floor(Math.random() * 100000) + 1;
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
                // console.log(extension);
                return time + random_digit + "." + extension;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function(file) {
                var name = file.upload.filename;
                let _token = "{{ csrf_token() }}";

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: name,
                        type: "local",
                        tmp_token: _token,

                    },
                    success: function(data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            success: function(file, response) {
                console.log(response);
            },
            error: function(file, response) {
                return false;
            }
        });

        //start  dropzone file remove 

        $(document).on('click', '.remove_uploaded_files', function(e) {
            var old_this = this;
            let fileName = $(this).data("id");
            if (fileName) {
                let _token = "{{ csrf_token() }}";
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('admin.recipeImageDelete') }}',
                    data: {
                        filename: fileName,
                        type: "edit_mode",
                        tmp_token: _token,
                    },
                    success: function(data) {
                        $(old_this).closest(".dz-image-preview").remove();
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });

            }
        });
        //end dropzone file remove 

        //start the add more direction 


        let direction_no = "{{ $last_keys_for_direction + 1 }}";
        $(document).on('click', '.btn_add_more_direction', function(e) {
            e.preventDefault();
            direction_no++;
            $(".appended_direction_recipe").append(`@include('admin.pages.recipe.add_more_direction')`);

            $("[name='description[" + direction_no + "]']").rules("add", {
                required: true,
                not_empty: true,
                minlength: 3,
                messages: {
                    required: "The sub category name field is required.",
                    not_empty: "only space is not allowed",
                }
            });

            var summernoteElement = $("#description_" + direction_no);
            var imagePath = 'summernote/recipe/images';
            summernoteElement.summernote({
                height: 300,
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        for (var i = files.length - 1; i >= 0; i--) {
                            sendFile(files[i], this, imagePath);
                        }
                    },
                    onMediaDelete: function(target) {
                        deleteFile(target[0].src);
                    },
                }
            });
        });
        //end the add more direction 

        $(document).on('click', '.btn_remove_direction', function(e) {
            e.preventDefault();
            $(this).closest(".div_remove_direction").remove();
        });
    </script>

    <script>
        $(document).ready(function() {
            jQuery.validator.setDefaults({
                ignore: [],
            });

            let which_step = "{{ session()->get('form_step_data') }}";
            if (which_step == "step_two_form") {
                $("#step_two_form").addClass("active");
                $("#step_two").addClass("active");
                $("#step_two").addClass("show");
                $("#form_step_first").removeClass("active show");
            } else if (which_step == "step_three_form") {
                $("#step_three_recipe_direction").addClass("active");
                $("#step_three").addClass("active");
                $("#step_three").addClass("show");
            } else {
                $("#step_first_form").addClass("active");
                $("#form_step_first").addClass("active");
                $("#form_step_first").addClass("show");

            }


            // $(".custom_select_two_class").select2();
            $("#frmEditRecipe").validate({
                rules: {
                    title: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                    },
                },
                messages: {
                    title: {
                        required: "@lang('validation.required', ['attribute' => 'Title'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Title'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Title', 'min' => 3])",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#frmEditIngredient").validate({
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#frmEditRecipeDirection").validate({
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            //start add more ingredient function  
            $("#add_more_btn").click(function(e) {
                e.preventDefault();
                addMoreIngredient();
            });
            let new_btn = "{{ $key + 1 }}";

            function addMoreIngredient() {
                // let start_btn = (($("select[name^=recipe_quantity_data]").length) + 1);
                let start_btn = new_btn++;
                // console.log(($("select[name^=recipe_quantity_data]").length);
                // var el1 = document.getElementById("appended_tags");
                // var sortable = Sortable.create(el1);

                $("#appended_tags").append(`@include('admin.pages.recipe.add_more_recipe_ingredient')`);
                // $(".custom_select_two_class").select2();
            }
            //end add more ingredient function 


            //alreay uploaded images find 
            uploaded_images();

            function uploaded_images() {
                $.ajax({
                    type: "get",
                    url: "{{ route('admin.uploaded_img') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "type": "edit_recipe_img",
                        "recipe_id": "{{ $recipe_data->id ?? '0' }}",
                    },

                    success: function(response) {
                        $("#already_uploaded_img").html(response);
                    }
                });
            }

            $(document).on('click', '.remove_btn_click', function(e) {
                e.preventDefault();
                $(this).closest(".ingredient_remove_div").remove();
            });


            $("#btn_update_recipe").click(function(e) {
                $('#frmEditRecipe').submit();
            });

            $('#frmEditRecipe').submit(function() {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });

            $('#frmEditIngredient').submit(function() {
                $("[name^=recipe_quantity_data]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "select quantity field is required.",
                        }
                    })
                });

                $("[name^=recipe_measurment_data]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "select measurment field is required.",
                        }
                    })
                });
                $("[name^=ingredients_name]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        messages: {
                            required: "select ingredient field is required.",
                        }
                    })
                });
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });


            $('#frmEditRecipeDirection').submit(function(e) {
                $("[name^=description]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        not_empty: true,
                        messages: {
                            required: "description field is required.",
                            not_empty: "only space is not allowed ",
                        }
                    })
                });

                $("[name^=title]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        not_empty: true,
                        alpha_numeric: true,
                        messages: {
                            required: "title field is required.",
                            not_empty: "only space is not allowed ",
                        }
                    })
                });
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
                // }
            });
            // 
            //ckeditor 
            let ck_editors = "{{ $last_keys_for_direction }}";
            // console.log(ck_editors);
            for (let index = 0; index <= ck_editors; index++) {
                var summernoteElement = $('#description_' + index);

                var imagePath = 'summernote/recipe/images';
                summernoteElement.summernote({
                    not_empty: true,
                    height: 300,
                    callbacks: {
                        onImageUpload: function(files, editor, welEditable) {
                            for (var i = files.length - 1; i >= 0; i--) {
                                sendFile(files[i], this, imagePath);
                            }
                        },
                        onMediaDelete: function(target) {
                            deleteFile(target[0].src);
                        },
                    }
                });

            }
            //
            //tell the validator to ignore Summernote elements
            $('form').each(function() {
                if ($(this).data('validator'))
                    $(this).data('validator').settings.ignore = ".note-editor *";
            });
        });
    </script>
@endpush
