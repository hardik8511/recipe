@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('category_update', $category->id) !!}
@endpush

@push('extra-css-styles')
    <style>
        strong.form-text {
            color: red !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-list text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">Edit {{ $custom_title }}</h3>
                </div>
            </div>

            <!--begin::Form-->
            <form id="frmEditCategory" method="POST" action="{{ route('admin.category.update', $category->custom_id) }}"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">
                    {{-- category Name --}}
                    <div class="form-group">
                        <label for="name"> {!! $mend_sign !!} Category Name:</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                            name="name" value="{{ old('name') != null ? old('name') : $category->name }}"
                            placeholder="Enter category name" autocomplete="name" spellcheck="false"
                            autocapitalize="sentences" tabindex="0" autofocus />
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class="form-text">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="sub_category">{!! $mend_sign !!} Sub Category Name:</label>
                        @if (!$sub_category_data->isEmpty())
                            @foreach ($sub_category_data as $key => $sub_category)
                                @php
                                    $start_list = ++$key;
                                @endphp
                                <div class="row sub_category_remove" style="{{ $key != '1' ? 'margin-top: 10px' : '' }}">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control sub_category_name_list"
                                            name="sub_category_name[{{ $key }}]"
                                            value=" {{ $sub_category->name }} " placeholder="Enter Sub Category name"
                                            spellcheck="false" autocapitalize="sentences" tabindex="0" />
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button"
                                            class="btn btn-{{ $key == '1' ? 'success' : 'danger' }} {{ $key == '1' ? 'btn_add_more' : 'btn_remove' }} ">
                                            <i class="fa {{ $key == '1' ? 'fa-plus' : 'fa-minus' }} ">
                                            </i> </button>
                                    </div>
                                </div>
                            @endforeach
                            <div id="dynamic_field">
                            </div>
                        @else
                            @php
                                $start_list = 1;
                            @endphp
                        @endif

                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">Update {{ $custom_title }}</button>
                    <a href="{{ route('admin.category.index') }}" class="btn btn-secondary">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('extra-js-scripts')
    <script>
        $(document).ready(function() {

            //validate the sub category 
            $("#frmEditCategory").validate({
                rules: {
                    name: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        only_alpha: true,
                        remote: {
                            url: "{{ route('admin.check.name') }}",
                            type: "post",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: "categories",
                                id: "{{ $category->id }}",
                            }
                        },
                    },
                    "sub_category_name[1]": {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        only_alpha: true,
                    },
                },
                messages: {
                    name: {
                        required: "@lang('validation.required', ['attribute' => 'name'])",
                        only_alpha: "Enter Valid Category Name",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'name'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'name', 'min' => 3])",
                        remote: "@lang('validation.unique', ['attribute' => 'categories'])",
                    },
                    "sub_category_name[1]": {
                        required: "@lang('validation.required', ['attribute' => 'sub category name'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'sub category name'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'sub category name', 'min' => 3])",
                        only_alpha: "Enter Valid Sub Category Name",
                    },
                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger');
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            //end of the validation

            //adding the rules when load data
            let start = {{ $start_list }};
            $(document).on('click', '.btn_add_more', function() {
                start++;
                let want_to_append_html =
                    '<div class="row sub_category_remove" style="margin-top: 10px"> <div class="col-md-9"> <input type="text" class="form-control sub_category_name_list" name="sub_category_name[' +
                    start +
                    ']"  placeholder="Enter Sub Category name" spellcheck="false" autocapitalize="sentences" tabindex="0" /> </div> <div class="col-md-2"> <button type="button" class="btn btn-danger btn_remove"> <i class="fa fa-minus"> </i> </button> </div> </div>';
                $('#dynamic_field').append(want_to_append_html);

                $("[name='sub_category_name[" + start + "]']").rules("add", {
                    required: true,
                    not_empty: true,
                    minlength: 3,
                    only_alpha: true,
                    messages: {
                        required: "The sub category name field is required.",
                        not_empty: "only space is not allowed",
                        only_alpha: "enter valid sub category name"
                    }
                });

            });

            $(document).on('click', '.btn_remove', function() {
                $(this).closest(".sub_category_remove").remove();
            });

            $('#frmEditCategory').submit(function() {
                $("[name^=sub_category_name]").each(function() {
                    $(this).rules('add', {
                        required: true,
                        only_alpha: true,
                        minlength: 3,
                        messages: {
                            required: "The sub category name field is required.",
                            minlength: "Enter at least {0} characters",
                            only_alpha: "enter valid sub category name"

                        }
                    })
                });
                // return false;

                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endpush
