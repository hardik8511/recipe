@extends('admin.layouts.app')

@push('breadcrumb')
    {!! Breadcrumbs::render('category_create') !!}
@endpush

@push('extra-css-styles')
    <style>
        strong.form-text {
            color: red !important;
        }
    </style>
@endpush
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-list text-primary"></i>
                    </span>
                    <h3 class="card-label text-uppercase">ADD {{ $custom_title }}</h3>
                </div>
            </div>

            <!--begin::Form-->
            <form id="frmAddUser" method="POST" action="{{ route('admin.category.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    {{-- First Name --}}
                    <div class="form-group">
                        <label for="name">{!! $mend_sign !!} Category Name:</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                            name="name" value="{{ old('name') }}" placeholder="Enter category name" autocomplete="name"
                            spellcheck="false" autocapitalize="sentences" tabindex="0" autofocus />
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class="form-text">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="sub_category">{!! $mend_sign !!} Sub Category Name:</label>
                        <div id="dynamic_field">
                            <div class="row">
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="sub_category_name[0]"
                                        placeholder="Enter Sub Category name" spellcheck="false" id="sub_category_name[0]"
                                        autocapitalize="sentences" tabindex="0" autofocus />
                                </div>

                                <div class="col-md-2">
                                    <button type="button" id="add" class="btn btn-success"> <i class="fa fa-plus">
                                        </i> </button>
                                </div>
                            </div>
                        </div>
                        @if ($errors->has('sub_category_name[0]'))
                            <span class="help-block">
                                <strong class="form-text">{{ $errors->first('sub_category_name[0]') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2 text-uppercase"> Add {{ $custom_title }}</button>
                    <a href="{{ route('admin.category.index') }}" class="btn btn-secondary text-uppercase">Cancel</a>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
@endsection

@push('extra-js-scripts')
    <script src="{{ asset('admin/js/custom.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            //validate mehtod to validate the category name 
            $("#frmAddUser").validate({
                rules: {
                    name: {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        maxlength: 50,
                        only_alpha: true,
                        remote: {
                            url: "{{ route('admin.check.name') }}",
                            type: "post",
                            data: {
                                _token: "{{ csrf_token() }}",
                                type: "categories",
                            }
                        },
                    },
                    "sub_category_name[0]": {
                        required: true,
                        not_empty: true,
                        minlength: 3,
                        maxlength: 50,
                        only_alpha: true,
                    }
                },
                messages: {
                    name: {
                        required: "@lang('validation.required', ['attribute' => 'Category Name'])",
                        only_alpha: "Enter Valid Category Name",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Category Name'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Category Name', 'min' => 3])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Category Name', 'min' => 50])",
                        remote: "@lang('validation.unique', ['attribute' => 'categories'])",
                    },
                    "sub_category_name[0]": {
                        required: "@lang('validation.required', ['attribute' => 'Sub Category Name'])",
                        not_empty: "@lang('validation.not_empty', ['attribute' => 'Sub Category Name'])",
                        minlength: "@lang('validation.min.string', ['attribute' => 'Sub Category Name', 'min' => 3])",
                        only_alpha: "Enter Valid Sub Category Name",
                        maxlength: "Maximum 50 Character Allowed"
                    },

                },
                errorClass: 'invalid-feedback',
                errorElement: 'span',
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                    $(element).siblings('label').addClass('text-danger'); // For Label
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                    $(element).siblings('label').removeClass('text-danger'); // For Label
                },
                errorPlacement: function(error, element) {
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            //end validate the category 

            //start of more functionality start         
            let start = 0;
            $('#add').click(function() {
                start++;
                let demo_html =
                    '<div class="row remove_sub_category" style="margin-top:10px"> <div class="col-md-9"> <input type="text" class="form-control" name="sub_category_name[' +
                    start +
                    ']" placeholder="Enter Sub Category name" spellcheck="false"  autocapitalize="sentences" tabindex="0" autofocus /> </div> <div class="col-md-2"> <button  type="button" id="add" class="btn btn-danger btn_remove"> <i class="fa fa-minus"> </i> </button> </div> </div>';

                $('#dynamic_field').append(demo_html);

                $("[name='sub_category_name[" + start + "]']").rules("add", {
                    required: true,
                    not_empty: true,
                    minlength: 3,
                    maxlength: 30,
                    only_alpha: true,
                    messages: {
                        required: "The Sub Category Name Field Is Required.",
                        not_empty: "Only Space Is Not Allowed",
                        only_alpha: "Enter Valid Sub Category Name",
                        maxlength: "Maximum 50 Character Allowed"
                    }
                });

            });

            $(document).on('click', '.btn_remove', function() {
                $(this).closest(".remove_sub_category").remove();
            });

            //end of add more functionality

            $('#frmAddUser').submit(function(e) {
                if ($(this).valid()) {
                    addOverlay();
                    $("input[type=submit], input[type=button], button[type=submit]").prop("disabled",
                        "disabled");
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endpush
